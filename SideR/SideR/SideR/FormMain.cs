﻿using SideR.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR
{
    public partial class FormMain : Form
    {
        public MojServer.MCClient mC = null;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            mC = new MojServer.MCClient();
            // Dummy call to server (so we establish an connection for fast use)
            mC.TestFunc();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (!proveriLoginData())
                return;
            FormGame frmGame = new FormGame();
            frmGame.mC = mC;
            frmGame.username = tbxUsername.Text;
            this.Visible = false;
            DialogResult dr = frmGame.ShowDialog();

            if (dr == DialogResult.OK)
            {
                tbxUsername.Text = "";
                tbxPassword.Text = "";
                this.Visible = true;
            }
        }

        private bool proveriLoginData()
        {
            if (String.IsNullOrWhiteSpace(tbxUsername.Text))
            {
                MessageBox.Show("Username cannot be blank.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (String.IsNullOrWhiteSpace(tbxPassword.Text))
            {
                MessageBox.Show("Password cannot be blank.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            bool status = mC.checkUser(tbxUsername.Text, tbxPassword.Text);
            if (!status)
                MessageBox.Show("Username and password do not match.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return status;
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            FormRegistration frmReg = new FormRegistration();
            frmReg.mC = mC;
            this.Visible = false;
            DialogResult dr = frmReg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                this.Visible = true;
            }
        }
        
    }
}
