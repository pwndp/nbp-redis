﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;
using SideR.Forms;

namespace SideR.Components
{
    public partial class ResourcePoolLocked : UserControl
    {
        private ResourcePool myRP = null;
        public FormGame gameForm { get; set; }

        public ResourcePool RP {
            get
            {
                return myRP;
            }
            set {
                myRP = value;
                lblResourceName.Text = myRP.Name;
            }
        }

        public ResourcePoolLocked()
        {
            InitializeComponent();
            myRP = null;
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            FormPurchase frmPch = new FormPurchase();
            frmPch.mC = gameForm.mC;
            frmPch.prices = RP.Price;
            frmPch.ItemName = "Resource: "+RP.Name;
            frmPch.ItemDesc = "";
            frmPch.gameForm = this.gameForm;
            frmPch.execPurchase = OnPurchase;
            frmPch.PurchaseData = PopulatePurchaseData;
            frmPch.UpdateComponents();

            DialogResult dr = frmPch.ShowDialog();

            if (dr == DialogResult.Yes)
            {
                gameForm.UpdateRPSlots();
            }
        }

        #region Purchasing an resource

        public Dictionary<string, byte[]> PopulatePurchaseData()
        {
            Dictionary<string, byte[]> data = new Dictionary<string, byte[]>();
            string res = "userLevel:" + gameForm.userLevel + "|workers:" +
                gameForm.freeWorkers + "|maxWorkers:" + gameForm.maxWorkers + "|";

            // Add code that decrements the player values
            List<Price> pr = RP.Price;
            for (var i = 0; i < pr.Count; i++)
            {
                foreach (var kv in gameForm.resursi)
                {
                    if (kv.Value.Name == pr[i].Name)
                    {
                        gameForm.resursi[kv.Key].UserAmount -= pr[i].Amount;
                        break;
                    }
                }
            }

            // First add all the unlocked resources
            foreach (var r in gameForm.resursi)
            {
                if (r.Value.IsUnlocked)
                {
                    res += "res" + r.Value.ID + ":" + r.Value.UserAmount + "|";
                }
            }
            // Then add the resource to be unlocked
            res += "res" + RP.ID + ":0|";

            data.Add("userdata", Encoding.UTF8.GetBytes(res));

            return data;
        }

        public bool OnPurchase(Dictionary<string, byte[]> data)
        {
            string userdata = "";
            data.TryGetValue("userdata", out byte[] binUserData);
            if (binUserData != null)
                userdata = Encoding.UTF8.GetString(binUserData);

            bool suc = gameForm.mC.setUserData(gameForm.username, userdata); 

            return suc;
        }

        #endregion
    }
}
