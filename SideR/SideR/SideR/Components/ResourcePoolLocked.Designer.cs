﻿namespace SideR.Components
{
    partial class ResourcePoolLocked
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxContainer = new System.Windows.Forms.GroupBox();
            this.btnUnlock = new System.Windows.Forms.Button();
            this.lblResourceName = new System.Windows.Forms.Label();
            this.gbxContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxContainer
            // 
            this.gbxContainer.Controls.Add(this.btnUnlock);
            this.gbxContainer.Controls.Add(this.lblResourceName);
            this.gbxContainer.Location = new System.Drawing.Point(8, 0);
            this.gbxContainer.Name = "gbxContainer";
            this.gbxContainer.Size = new System.Drawing.Size(528, 96);
            this.gbxContainer.TabIndex = 0;
            this.gbxContainer.TabStop = false;
            // 
            // btnUnlock
            // 
            this.btnUnlock.Location = new System.Drawing.Point(218, 53);
            this.btnUnlock.Name = "btnUnlock";
            this.btnUnlock.Size = new System.Drawing.Size(97, 28);
            this.btnUnlock.TabIndex = 11;
            this.btnUnlock.Text = "Unlock";
            this.btnUnlock.UseVisualStyleBackColor = true;
            this.btnUnlock.Click += new System.EventHandler(this.btnUnlock_Click);
            // 
            // lblResourceName
            // 
            this.lblResourceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResourceName.Location = new System.Drawing.Point(197, 21);
            this.lblResourceName.Name = "lblResourceName";
            this.lblResourceName.Size = new System.Drawing.Size(145, 20);
            this.lblResourceName.TabIndex = 10;
            this.lblResourceName.Text = "Name_of_resource";
            this.lblResourceName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ResourcePoolLocked
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbxContainer);
            this.Name = "ResourcePoolLocked";
            this.Size = new System.Drawing.Size(543, 102);
            this.gbxContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxContainer;
        private System.Windows.Forms.Label lblResourceName;
        private System.Windows.Forms.Button btnUnlock;
    }
}
