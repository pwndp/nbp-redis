﻿namespace SideR.Components
{
    partial class UpgradeComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBuy = new System.Windows.Forms.Button();
            this.lblUpgradeName = new System.Windows.Forms.Label();
            this.lblLevelUpgrade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBuy
            // 
            this.btnBuy.Location = new System.Drawing.Point(217, 17);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(66, 28);
            this.btnBuy.TabIndex = 0;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // lblUpgradeName
            // 
            this.lblUpgradeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpgradeName.Location = new System.Drawing.Point(3, 17);
            this.lblUpgradeName.Name = "lblUpgradeName";
            this.lblUpgradeName.Size = new System.Drawing.Size(208, 28);
            this.lblUpgradeName.TabIndex = 1;
            this.lblUpgradeName.Text = "NameOfUpgrade";
            this.lblUpgradeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLevelUpgrade
            // 
            this.lblLevelUpgrade.Location = new System.Drawing.Point(171, 25);
            this.lblLevelUpgrade.Name = "lblLevelUpgrade";
            this.lblLevelUpgrade.Size = new System.Drawing.Size(40, 13);
            this.lblLevelUpgrade.TabIndex = 2;
            this.lblLevelUpgrade.Text = "99/99";
            this.lblLevelUpgrade.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UpgradeComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblLevelUpgrade);
            this.Controls.Add(this.lblUpgradeName);
            this.Controls.Add(this.btnBuy);
            this.Name = "UpgradeComponent";
            this.Size = new System.Drawing.Size(301, 63);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Label lblUpgradeName;
        private System.Windows.Forms.Label lblLevelUpgrade;
    }
}
