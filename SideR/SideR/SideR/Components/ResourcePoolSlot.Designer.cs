﻿namespace SideR.Components
{
    partial class ResourcePoolSlot
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSlot = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlSlot
            // 
            this.pnlSlot.BackColor = System.Drawing.SystemColors.Control;
            this.pnlSlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSlot.Location = new System.Drawing.Point(0, 0);
            this.pnlSlot.Name = "pnlSlot";
            this.pnlSlot.Size = new System.Drawing.Size(543, 102);
            this.pnlSlot.TabIndex = 0;
            // 
            // ResourcePoolSlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.pnlSlot);
            this.Name = "ResourcePoolSlot";
            this.Size = new System.Drawing.Size(543, 102);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSlot;
    }
}
