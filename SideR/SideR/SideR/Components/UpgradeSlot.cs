﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;

namespace SideR.Components
{
    public partial class UpgradeSlot : UserControl
    {
        Upgrade _upgrade = null;
        public Upgrade Upgrade
        {
            get
            {
                return _upgrade;
            }
            set
            {
                _upgrade = value;
                SetData();
            }
        }
        public Form MyFormParent { get; set; }

        public UpgradeSlot()
        {
            InitializeComponent();
        }

        private void SetData()
        {
            pnlContainer.Controls.Clear();
            if (_upgrade != null)
            {
                UpgradeComponent uc = new UpgradeComponent();
                uc.MyFormParent = this.MyFormParent;
                uc.Location = new Point(0, 0);
                uc.Upgrade = this.Upgrade;
                pnlContainer.Controls.Add(uc);
            }
        }

    }
}
