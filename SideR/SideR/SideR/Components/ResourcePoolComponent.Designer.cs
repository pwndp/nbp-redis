﻿namespace SideR.Components
{
    partial class ResourcePoolComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbxResourcePool = new System.Windows.Forms.GroupBox();
            this.lblResourceName = new System.Windows.Forms.Label();
            this.lblPerWorkerAmount = new System.Windows.Forms.Label();
            this.pbr_leftResources = new System.Windows.Forms.ProgressBar();
            this.tbxNumOfWorkers = new System.Windows.Forms.TextBox();
            this.lblTimer = new System.Windows.Forms.Label();
            this.btnUpgradeRP = new System.Windows.Forms.Button();
            this.btnDonate = new System.Windows.Forms.Button();
            this.btnMine = new System.Windows.Forms.Button();
            this.tmrCooldown = new System.Windows.Forms.Timer(this.components);
            this.gbxResourcePool.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxResourcePool
            // 
            this.gbxResourcePool.Controls.Add(this.lblResourceName);
            this.gbxResourcePool.Controls.Add(this.lblPerWorkerAmount);
            this.gbxResourcePool.Controls.Add(this.pbr_leftResources);
            this.gbxResourcePool.Controls.Add(this.tbxNumOfWorkers);
            this.gbxResourcePool.Controls.Add(this.lblTimer);
            this.gbxResourcePool.Controls.Add(this.btnUpgradeRP);
            this.gbxResourcePool.Controls.Add(this.btnDonate);
            this.gbxResourcePool.Controls.Add(this.btnMine);
            this.gbxResourcePool.Location = new System.Drawing.Point(8, 0);
            this.gbxResourcePool.Name = "gbxResourcePool";
            this.gbxResourcePool.Size = new System.Drawing.Size(528, 96);
            this.gbxResourcePool.TabIndex = 0;
            this.gbxResourcePool.TabStop = false;
            // 
            // lblResourceName
            // 
            this.lblResourceName.Location = new System.Drawing.Point(35, 27);
            this.lblResourceName.Name = "lblResourceName";
            this.lblResourceName.Size = new System.Drawing.Size(97, 13);
            this.lblResourceName.TabIndex = 9;
            this.lblResourceName.Text = "Name_of_resource";
            this.lblResourceName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPerWorkerAmount
            // 
            this.lblPerWorkerAmount.Location = new System.Drawing.Point(201, 28);
            this.lblPerWorkerAmount.Name = "lblPerWorkerAmount";
            this.lblPerWorkerAmount.Size = new System.Drawing.Size(106, 13);
            this.lblPerWorkerAmount.TabIndex = 8;
            this.lblPerWorkerAmount.Text = "AmountPerWorker";
            this.lblPerWorkerAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbr_leftResources
            // 
            this.pbr_leftResources.Cursor = System.Windows.Forms.Cursors.Default;
            this.pbr_leftResources.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbr_leftResources.Location = new System.Drawing.Point(12, 55);
            this.pbr_leftResources.MarqueeAnimationSpeed = 100000000;
            this.pbr_leftResources.Name = "pbr_leftResources";
            this.pbr_leftResources.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pbr_leftResources.RightToLeftLayout = true;
            this.pbr_leftResources.Size = new System.Drawing.Size(145, 21);
            this.pbr_leftResources.Step = 50;
            this.pbr_leftResources.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbr_leftResources.TabIndex = 7;
            this.pbr_leftResources.Value = 50;
            // 
            // tbxNumOfWorkers
            // 
            this.tbxNumOfWorkers.Location = new System.Drawing.Point(185, 57);
            this.tbxNumOfWorkers.MaxLength = 9;
            this.tbxNumOfWorkers.Name = "tbxNumOfWorkers";
            this.tbxNumOfWorkers.Size = new System.Drawing.Size(131, 20);
            this.tbxNumOfWorkers.TabIndex = 6;
            this.tbxNumOfWorkers.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbxNumOfWorkers.TextChanged += new System.EventHandler(this.tbxNumOfWorkers_TextChanged);
            this.tbxNumOfWorkers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNumOfWorkers_KeyPress);
            // 
            // lblTimer
            // 
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.Location = new System.Drawing.Point(345, 59);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(58, 20);
            this.lblTimer.TabIndex = 5;
            this.lblTimer.Text = "AB:CD";
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpgradeRP
            // 
            this.btnUpgradeRP.Location = new System.Drawing.Point(335, 19);
            this.btnUpgradeRP.Name = "btnUpgradeRP";
            this.btnUpgradeRP.Size = new System.Drawing.Size(77, 31);
            this.btnUpgradeRP.TabIndex = 4;
            this.btnUpgradeRP.Text = "Upgrade";
            this.btnUpgradeRP.UseVisualStyleBackColor = true;
            this.btnUpgradeRP.Click += new System.EventHandler(this.btnUpgradeRP_Click);
            // 
            // btnDonate
            // 
            this.btnDonate.Location = new System.Drawing.Point(434, 18);
            this.btnDonate.Name = "btnDonate";
            this.btnDonate.Size = new System.Drawing.Size(77, 31);
            this.btnDonate.TabIndex = 3;
            this.btnDonate.Text = "Donate";
            this.btnDonate.UseVisualStyleBackColor = true;
            this.btnDonate.Click += new System.EventHandler(this.btnDonate_Click);
            // 
            // btnMine
            // 
            this.btnMine.Location = new System.Drawing.Point(434, 55);
            this.btnMine.Name = "btnMine";
            this.btnMine.Size = new System.Drawing.Size(77, 31);
            this.btnMine.TabIndex = 2;
            this.btnMine.Text = "Mine";
            this.btnMine.UseVisualStyleBackColor = true;
            this.btnMine.Click += new System.EventHandler(this.btnMine_Click);
            // 
            // tmrCooldown
            // 
            this.tmrCooldown.Interval = 1000;
            this.tmrCooldown.Tick += new System.EventHandler(this.tmrCooldown_Tick);
            // 
            // ResourcePoolComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbxResourcePool);
            this.Name = "ResourcePoolComponent";
            this.Size = new System.Drawing.Size(543, 102);
            this.gbxResourcePool.ResumeLayout(false);
            this.gbxResourcePool.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxResourcePool;
        private System.Windows.Forms.Button btnDonate;
        private System.Windows.Forms.Button btnMine;
        private System.Windows.Forms.Button btnUpgradeRP;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.TextBox tbxNumOfWorkers;
        private System.Windows.Forms.ProgressBar pbr_leftResources;
        private System.Windows.Forms.Label lblPerWorkerAmount;
        private System.Windows.Forms.Label lblResourceName;
        private System.Windows.Forms.Timer tmrCooldown;
    }
}
