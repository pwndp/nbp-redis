﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;
using SideR.Forms;

namespace SideR.Components
{
    public partial class ResourcePoolSlot : UserControl
    {
        public int slotID = 0; // The id of the slot
        int RPID = -1; // The id of the resource pool this slot is holding
        bool isUnlocked = false; // Is this RP unlocked
        ResourcePool reference = null;
        public FormGame gameForm { get; set; }

        public ResourcePool RP
        {
            get
            {
                return reference;
            }
            set
            {
                reference = value;
            }
        }

        Point objPosition = new Point(0, 0);
        Size objSize = new Size(543, 102);

        public ResourcePoolSlot()
        {
            InitializeComponent();
            checkStatusChaged();
        }

        private void SetUnlockState(bool state=false)
        {
            isUnlocked = state;
            checkStatusChaged();
        }

        private void checkStatusChaged()
        {
            pnlSlot.Controls.Clear();
            if (reference != null)
            {
                if (isUnlocked)
                {
                    // Instantiate the ResourcePoolComponent
                    ResourcePoolComponent rpc = new ResourcePoolComponent();
                    rpc.gameForm = this.gameForm;
                    rpc.RP = reference;
                    rpc.Size = objSize;
                    rpc.Location = objPosition;
                    pnlSlot.Controls.Add(rpc);
                }
                else
                {
                    // Instantiate the ResourcePoolLocked
                    ResourcePoolLocked rpl = new ResourcePoolLocked();
                    rpl.gameForm = gameForm;
                    rpl.RP = reference;
                    rpl.Size = objSize;
                    rpl.Location = objPosition;
                    pnlSlot.Controls.Add(rpl);
                }
            }
        }

        public void SetAndRefresh()
        {
            if (reference != null)
            {
                isUnlocked = reference.IsUnlocked;
                RPID = reference.ID;
            }
            else
            {
                isUnlocked = false;
                RPID = -1;
            }

            checkStatusChaged();
        }
    }
}
