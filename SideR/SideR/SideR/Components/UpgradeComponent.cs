﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;
using SideR.Forms;

namespace SideR.Components
{
    public partial class UpgradeComponent : UserControl
    {
        Upgrade upgrade;
        public Upgrade Upgrade
        {
            get
            {
                return upgrade;
            }
            set
            {
                upgrade = value;
                SetData();
            }
        }
        bool isWorkerUpgrade = false;
        public Form MyFormParent { get; set; }

        private void SetData()
        {
            if (upgrade != null)
            {
                string upname = Upgrade.Name.ToLower();
                lblUpgradeName.Text = upgrade.Name;

                // For non-Worker upgrades display something different
                if (!upname.Contains("worker") && !upname.Contains("workers"))
                {
                    int valToDisplay = (upgrade.GetPrices() != null) ? (upgrade.Level - 1) : upgrade.MaxLevel;
                    lblLevelUpgrade.Text = "" + valToDisplay + "/" + upgrade.MaxLevel;
                    if (valToDisplay == upgrade.MaxLevel)
                    {
                        btnBuy.Enabled = false;
                        btnBuy.Text = "Max";
                    }
                    isWorkerUpgrade = false;
                }
                else
                {
                    lblLevelUpgrade.Text = "";
                    isWorkerUpgrade = true;
                }
            }
        }

        public UpgradeComponent()
        {
            InitializeComponent();
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            FormUpgrades par = (FormUpgrades)MyFormParent;
            FormPurchase frmPch = new FormPurchase();
            frmPch.mC = par.mC;
            frmPch.ItemName = Upgrade.Name;
            frmPch.gameForm = par.GameForm;

            if (!isWorkerUpgrade)
            {
                // Non-worker upgrade
                frmPch.prices = Upgrade.GetPrices();
                frmPch.ItemDesc = "Level " + Upgrade.Level;
                frmPch.execPurchase = OnPurchase;
                frmPch.PurchaseData = PopulatePurchaseData;
            }
            else
            {
                // Worker upgrade
                FormGame gf = ((FormUpgrades)MyFormParent).GameForm;
                int maxworkers = gf.maxWorkers;
                frmPch.ItemDesc = "";
                List<Price> upgs = Upgrade.GetPrices();
                int multiplier = gf.mC.getWorkerUpgradeDataValue(gf.username, Upgrade.Name)+1;
                if (Upgrade.Name.ToLower() == "buy a worker")
                    multiplier -= 1;
                for(int i = 0; i < upgs.Count; i++)
                {
                    upgs[i].Amount *= multiplier;
                }
                frmPch.prices = upgs;
                // Set new behaviours
                frmPch.execPurchase = OnPurchaseWorker;
                frmPch.PurchaseData = PopulateWorkerPurchaseData;
            }

            frmPch.UpdateComponents();

            DialogResult dr = frmPch.ShowDialog();
            if (dr == DialogResult.Yes)
            {
                // After the purchase was successfully done, the screen is refreshed
                // and the upgrades are also updated. That means that we can reasign
                // the values from the main screen to the update form
                FormUpgrades frm = (FormUpgrades)MyFormParent;
                frm.Upgrades = frm.GameForm.Upgrades;
            }
        }

        #region Purchasing an upgrade

        private Dictionary<string, byte[]> PopulatePurchaseData()
        {
            Dictionary<string, byte[]> pData = new Dictionary<string, byte[]>();
            List<Upgrade> ups = ((FormUpgrades)MyFormParent).GetUpgradesList();
            FormUpgrades upgFrm = (FormUpgrades)MyFormParent;
            FormGame frm = upgFrm.GameForm;

            // Add every upgrade that was purchased at least once (but not this)
            string res = "";
            foreach (Upgrade up in ups)
            {
                if (up.Level > 1 && up.Name!=upgrade.Name)
                    res += up.Name + ":" + up.Level + "|";
            }
            // And then add this upgrade
            res += upgrade.Name + ":" + (upgrade.Level + 1) + "|";
            pData.Add("upgradesData", Encoding.UTF8.GetBytes(res));

            // Increase player level because he bought an upgrade
            frm.userLevel++;

            // Add code that decrements the player values
            List<Price> pr = Upgrade.GetPrices();
            res = "userLevel:"+frm.userLevel+ "|workers:"+frm.freeWorkers+ "|maxWorkers:"+frm.maxWorkers+"|";
            for(var i=0;i<pr.Count;i++)
            {
                foreach(var kv in frm.resursi)
                {
                    if (kv.Value.Name == pr[i].Name)
                    {
                        frm.resursi[kv.Key].UserAmount -= pr[i].Amount;
                        break;
                    }
                }    
            }
            // Now, save all the unlocked resources and their values
            foreach(var kv in frm.resursi)
            {
                if (kv.Value.IsUnlocked)
                    res += "res"+kv.Value.ID+":"+kv.Value.UserAmount+"|";
            }
            pData.Add("userData", Encoding.UTF8.GetBytes(res));

            return pData;
        }

        private bool OnPurchase(Dictionary<string,byte[]> data)
        {
            FormUpgrades frm = (FormUpgrades)MyFormParent;
            string upgrades = "";
            string userdata = "";

            data.TryGetValue("upgradesData", out byte[] binUpgradeData);
            if (binUpgradeData != null)
                upgrades = Encoding.UTF8.GetString(binUpgradeData);

            data.TryGetValue("userData", out byte[] binUserData);
            if (binUserData != null)
                userdata = Encoding.UTF8.GetString(binUserData);

            bool suc = frm.mC.setUserUpgrades(frm.GameForm.username, upgrades);
            bool suc2 = frm.mC.setUserData(frm.GameForm.username, userdata);
            
            return suc & suc2;
        }

        private Dictionary<string,byte[]> PopulateWorkerPurchaseData()
        {
            Dictionary<string, byte[]> dict = new Dictionary<string, byte[]>();
            FormGame frm = ((FormUpgrades)MyFormParent).GameForm;

            string upgdata = frm.mC.GetWorkerUpgradeData(frm.username);
            int cUpgLvl = frm.mC.getWorkerUpgradeDataValue(frm.username, Upgrade.Name);
            string resdata = "";

            if (!String.IsNullOrWhiteSpace(upgdata))
            {
                string[] spl = upgdata.Split('|');
                foreach(string sp in spl)
                {
                    if (sp != "")
                    {
                        string[] vk = sp.Split(':');
                        if (vk[0] != Upgrade.Name)
                            resdata += "" + vk[0] + ":" + vk[1] + "|";
                    }
                }
            }
            resdata += "" + Upgrade.Name + ":" + (cUpgLvl + 1) + "|";

            dict.Add("wudata", Encoding.UTF8.GetBytes(resdata));

            return dict;
        }

        private bool OnPurchaseWorker(Dictionary<string,byte[]> data)
        {
            FormGame frm = ((FormUpgrades)MyFormParent).GameForm;
            data.TryGetValue("wudata", out byte[] binWData);
            string wdata = "";
            if (binWData != null)
                wdata = Encoding.UTF8.GetString(binWData);

            bool suc = frm.mC.SetWorkerUpgradeData(frm.username, wdata);

            // Increase user level, because he bought an upgrade
            if (suc)
                frm.userLevel++;

            if (Upgrade.Name == "Buy a worker")
            {
                frm.freeWorkers++;
                frm.maxWorkers++;
            }

            List<Price> pr = Upgrade.GetPrices();
            string res = "userLevel:" + frm.userLevel + "|workers:" + frm.freeWorkers + "|maxWorkers:" + frm.maxWorkers + "|";
            for (var i = 0; i < pr.Count; i++)
            {
                foreach (var kv in frm.resursi)
                {
                    if (kv.Value.Name == pr[i].Name)
                    {
                        frm.resursi[kv.Key].UserAmount -= pr[i].Amount;
                        break;
                    }
                }
            }

            frm.mC.setUserData(frm.username, frm.generateUserData());

            return suc;
        }

        #endregion

    }
}
