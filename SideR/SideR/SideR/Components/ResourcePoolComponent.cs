﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;
using SideR.Forms;

namespace SideR.Components
{
    public partial class ResourcePoolComponent : UserControl
    {

        private ResourcePool myRP = null;
        public ResourcePool RP
        {
            get
            {
                return myRP;
            }
            set
            {
                myRP = value;
                InitComponents();
            }
        }
        public FormGame gameForm { get; set; }
        public int miningAmount;
        int seconds = 0;
        int minutes = 0;
        int bonus = 0;

        public ResourcePoolComponent()
        {
            InitializeComponent();
            myRP = null;
            miningAmount = 0;
            UpdateProgressBar();
        }

        private void InitComponents()
        {
            tmrCooldown.Enabled = true;
            btnMine.Enabled = false;
            tmrCooldown.Start();
            lblResourceName.Text = RP.Name;
            bonus=gameForm.mC.getRPPerWorkerUpgrade(gameForm.username, RP.Name);
            lblPerWorkerAmount.Text = "Per worker: "+(RP.PerWorkerAmount+bonus).ToString();
            seconds = 0;
            minutes = 0;
            // Check time until expiral
            DateTime endVal = gameForm.mC.GetUserTimedData(gameForm.username, RP.ID);
            if (endVal != null)
            {
                TimeSpan span = endVal - DateTime.Now;
                if (span.TotalSeconds > 0)
                {
                    int total = (int)span.TotalSeconds;
                    minutes = total / 60;
                    seconds = total - minutes * 60;
                }
            }
            if (seconds < 0)
                seconds = 0;
            if (minutes < 0)
                minutes = 0;
            checkButtonUnlock();
            lblTimer.Text = ""+minutes+":"+seconds;
            // Progress bar
            UpdateProgressBar();
        }

        public void UpdateProgressBar()
        {
            float perc = 1;
            if (gameForm!=null && gameForm.resCapacity != null) { 
                if (gameForm.resCapacity.ContainsKey(RP.ID))
                {
                    int[] kv = gameForm.resCapacity[RP.ID];
                    perc = kv[0] / (float)kv[1];
                }
            }
            int progress = (int) (100*perc);
            // Clamp the value
            if (progress < 1)
                progress = 1;
            else if (progress > 100)
                progress = 100;
            // Display the value
            pbr_leftResources.Minimum = 0;
            pbr_leftResources.Value = progress;
            pbr_leftResources.Value = progress-1;
        }

        private void tbxNumOfWorkers_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
        }

        private void btnDonate_Click(object sender, EventArgs e)
        {
            FormDonation frmDon = new FormDonation();
            frmDon.ResPool = RP;
            frmDon.gameForm = this.gameForm;

            DialogResult dr = frmDon.ShowDialog();
            if (dr == DialogResult.Yes)
                gameForm.UpdateRPSlots();
        }

        private void btnMine_Click(object sender, EventArgs e)
        {
            if (!checkFields())
                return;

            CalculateAmount();
            ResourcePool rp = gameForm.mC.MiningDonating(RP.ID, miningAmount, true);
            if (rp != null)
            {
                // Calculate the wait time
                int poolLvl = gameForm.mC.GetResourcePoolLevel(RP.ID);
                int upgradeLvl = gameForm.mC.getUserUpgradeLevel(gameForm.username, RP.Name) - 1;
                int timePerPool = (rp.ID + 1) * 30+(poolLvl-1)*15;
                int decreace = (timePerPool / 10) * upgradeLvl;
                timePerPool -= decreace;

                minutes = timePerPool / 60;
                seconds = timePerPool - (minutes*60);
                DateTime ndt = DateTime.Now.AddMinutes(minutes).AddSeconds(seconds);

                // Apply changes and save states
                int workerNum = Int32.Parse(tbxNumOfWorkers.Text);
                gameForm.freeWorkers -= workerNum;
                gameForm.refreshWorkers();
                btnMine.Enabled = false;
                gameForm.resursi[rp.ID].UserAmount += rp.UserAmount;
                gameForm.setupResourcesPanel();
                bool suc = gameForm.mC.SetUserTimedData(gameForm.username, RP.ID, workerNum, ndt);
                bool suc2 = gameForm.mC.setUserData(gameForm.username, gameForm.generateUserData());
                gameForm.UpdateRPSlots();
                UpdateProgressBar();

                // Update the worker number (again)
                gameForm.freeWorkers -= workerNum;
                gameForm.refreshWorkers();
            }

        }

        private bool checkFields()
        {
            if (String.IsNullOrWhiteSpace(tbxNumOfWorkers.Text))
            {
                MessageBox.Show("Please assign workers to perform resource gathering.");
                return false;
            }
            return true;
        }

        private void CalculateAmount()
        { 
            int workerNum = Int32.Parse(tbxNumOfWorkers.Text);
            int amnt = RP.PerWorkerAmount + bonus;

            miningAmount = workerNum*amnt;
        }

        private void tmrCooldown_Tick(object sender, EventArgs e)
        {
            if (seconds > 0)
                seconds--;

            if (minutes > 0)
                if (seconds <= 0) { 
                    minutes--;
                    seconds = 59;
                }

            // Fix refresh bug (if happens)
            if (seconds < 0)
                seconds = 0;
            if (minutes < 0)
                minutes = 0;

            lblTimer.Text = minutes + ":" + seconds;
            checkButtonUnlock();
        }

        private void checkButtonUnlock()
        {
            if (minutes <=0 && seconds <= 0)
            {
                int workerNum = gameForm.mC.GetUserBussyWorkers(gameForm.username, RP.ID);
                gameForm.freeWorkers += workerNum;
                gameForm.refreshWorkers();
                btnMine.Enabled = true;
                tmrCooldown.Enabled = false;
            }
        }

        private void btnUpgradeRP_Click(object sender, EventArgs e)
        {
            FormUpgradeRP frmUpg = new FormUpgradeRP();
            frmUpg.gameForm = this.gameForm;
            frmUpg.RP = this.RP;

            frmUpg.ShowDialog();
        }

        private void tbxNumOfWorkers_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbxNumOfWorkers.Text))
            {
                int num = Int32.Parse(tbxNumOfWorkers.Text);
                if (num > this.gameForm.freeWorkers)
                    num = this.gameForm.freeWorkers;

                tbxNumOfWorkers.Text = num.ToString();
            }
        }
    }
}
