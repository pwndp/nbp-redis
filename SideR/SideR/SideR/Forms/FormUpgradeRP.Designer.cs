﻿namespace SideR.Forms
{
    partial class FormUpgradeRP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxAmount = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lblTx1 = new System.Windows.Forms.Label();
            this.lblTx2 = new System.Windows.Forms.Label();
            this.lblRemainingAmount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbxAmount
            // 
            this.tbxAmount.Location = new System.Drawing.Point(48, 101);
            this.tbxAmount.Margin = new System.Windows.Forms.Padding(2);
            this.tbxAmount.Name = "tbxAmount";
            this.tbxAmount.Size = new System.Drawing.Size(81, 20);
            this.tbxAmount.TabIndex = 7;
            this.tbxAmount.TextChanged += new System.EventHandler(this.tbxAmount_TextChanged);
            this.tbxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAmount_KeyPress);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(139, 97);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(56, 25);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // lblTx1
            // 
            this.lblTx1.AutoSize = true;
            this.lblTx1.Location = new System.Drawing.Point(44, 20);
            this.lblTx1.Name = "lblTx1";
            this.lblTx1.Size = new System.Drawing.Size(156, 13);
            this.lblTx1.TabIndex = 8;
            this.lblTx1.Text = "Remaining amount of resources";
            // 
            // lblTx2
            // 
            this.lblTx2.AutoSize = true;
            this.lblTx2.Location = new System.Drawing.Point(24, 37);
            this.lblTx2.Name = "lblTx2";
            this.lblTx2.Size = new System.Drawing.Size(198, 13);
            this.lblTx2.TabIndex = 9;
            this.lblTx2.Text = "until the upgrade of the Resource pool is";
            // 
            // lblRemainingAmount
            // 
            this.lblRemainingAmount.Location = new System.Drawing.Point(64, 64);
            this.lblRemainingAmount.Name = "lblRemainingAmount";
            this.lblRemainingAmount.Size = new System.Drawing.Size(105, 18);
            this.lblRemainingAmount.TabIndex = 10;
            this.lblRemainingAmount.Text = "999999/99999";
            this.lblRemainingAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormUpgradeRP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 152);
            this.Controls.Add(this.lblRemainingAmount);
            this.Controls.Add(this.lblTx2);
            this.Controls.Add(this.lblTx1);
            this.Controls.Add(this.tbxAmount);
            this.Controls.Add(this.btnConfirm);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUpgradeRP";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upgrade Resources";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbxAmount;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label lblTx1;
        private System.Windows.Forms.Label lblTx2;
        private System.Windows.Forms.Label lblRemainingAmount;
    }
}