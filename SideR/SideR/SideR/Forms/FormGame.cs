﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Server.Entities;
using SideR.Components;

namespace SideR.Forms
{
    public partial class FormGame : Form
    {
        #region Fields

        public MojServer.MCClient mC = null;
        public Dictionary<int, ResourcePool> resursi = null;
        public Dictionary<int, int[]> resCapacity = null;
        public List<Upgrade> Upgrades = null;
        public string username;
        public int userLevel;
        public int freeWorkers;
        public int maxWorkers;
        int refreshInterval;
        int refreshButonInterval;
        const int longRefreshInterval = 10;
        const int shortRefreshInterval = 5;
        int curPage;
        int maxPage;

        #endregion

        #region Called only once

        public FormGame()
        {
            InitializeComponent();
            InitVars();
        }

        private void InitVars()
        {
            username = "";
            userLevel = 1;
            freeWorkers = maxWorkers = 0;
            curPage = maxPage = 0;
            rpSlot1.slotID = 0;
            rpSlot2.slotID = 1;
            rpSlot3.slotID = 2;
            rpSlot1.gameForm = this;
            rpSlot2.gameForm = this;
            rpSlot3.gameForm = this;
            refreshInterval = longRefreshInterval;
            refreshButonInterval = -1;
            resCapacity = new Dictionary<int, int[]>();
        }

        private void loadResPools()
        {
            ResourcePool[] pls = mC.GetResourcePools();
            resursi = new Dictionary<int, ResourcePool>();
            foreach (ResourcePool rp in pls)
            {
                if (!resursi.ContainsKey(rp.ID))
                {
                    resursi.Add(rp.ID, rp);
                }
            }

            UpdateRPSlots();
            parseUserData();
        }

        private void loadUpgrades()
        {
            Upgrades = mC.getUserUpgrades(username).ToList();
            // This MUST be changed
            for (int i = 0; i < Upgrades.Count; i++)
            {
                // Check for prices only for these upgrades that are not max level
                if (Upgrades[i].Level <= Upgrades[i].MaxLevel)
                {
                    ItemPrice ip = mC.getItemPrice(Upgrades[i].Name, Upgrades[i].Level);
                    Upgrades[i].SetPrices((ip != null) ? ip.GetPrices() : null);
                }
            }
        }

        private void parseUserData()
        {
            string dat = mC.getUserData(username);
            string[] fls = dat.Split('|');
            foreach (var f in fls)
            {
                string[] kv = f.Split(':');
                      //string t = "userLevel:1|workers:0|maxWorkers:0|res0:0|res1:0|";
                if (kv[0] == "userLevel")
                    userLevel = Int32.Parse(kv[1]);
                else if (kv[0] == "workers")
                    freeWorkers = Int32.Parse(kv[1]);
                else if (kv[0] == "maxWorkers")
                    maxWorkers = Int32.Parse(kv[1]);
                else
                {
                    if (kv[0] != "")
                    {
                        int val = Int32.Parse(kv[1]);
                        int idRes = Int32.Parse(kv[0].Substring(3)); // zanemari 'res' deo
                        if (resursi.ContainsKey(idRes))
                        {
                            resursi[idRes].IsUnlocked = true;
                            resursi[idRes].UserAmount = val;
                        }
                    }
                }
            }
        }

        private void FormGame_Load(object sender, EventArgs e)
        {
            // Load RPs
            loadResPools();
            loadUpgrades();
            maxPage = (int)Math.Ceiling(resursi.Count / (float)3) - 1;
            // Setup components
            btnPreviousPage.Visible = false;
            btnPreviousPage.Enabled = false;
            if (maxPage == curPage)
            {
                btnNextPage.Visible = false;
                btnNextPage.Enabled = false;
            }

            setupResourcesPanel();

            pnlResources.AutoScroll = false;
            pnlResources.HorizontalScroll.Enabled = false;
            pnlResources.HorizontalScroll.Visible = false;
            pnlResources.HorizontalScroll.Maximum = 0;
            pnlResources.AutoScroll = true;
            // Then refresh data on screen
            RefreshDataOnScreen();
            tmrRR.Start();
        }
        
        #endregion

        #region Events

        private void FormGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to exit?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                tmrRR.Stop();
                DialogResult = DialogResult.OK;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshDataOnScreen();
            refreshButonInterval = 2;
            btnRefresh.Enabled = false;
        }

        private void btnUpgrades_Click(object sender, EventArgs e)
        {
            FormUpgrades frmUpg = new FormUpgrades();
            frmUpg.mC = this.mC;
            frmUpg.Upgrades = this.Upgrades;
            frmUpg.GameForm = this;
            this.Visible = false;
            DialogResult dr = frmUpg.ShowDialog();

            if (dr == DialogResult.OK)
            {
                this.Visible = true;
            }

        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            if (curPage < maxPage)
                curPage++;

            UpdateRPSlots();

            if (curPage == maxPage)
            {
                btnNextPage.Visible = false;
                btnNextPage.Enabled = false;
            }

            if (curPage == 0)
            {
                btnPreviousPage.Visible = false;
                btnPreviousPage.Enabled = false;
            }
            else
            {
                btnPreviousPage.Visible = true;
                btnPreviousPage.Enabled = true;
            }
        }

        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            if (curPage > 0)
                curPage--;

            UpdateRPSlots();

            if (curPage == 0)
            {
                btnPreviousPage.Visible = false;
                btnPreviousPage.Enabled = false;
            }

            if (curPage == maxPage)
            {
                btnNextPage.Visible = false;
                btnNextPage.Enabled = false;
            }
            else
            {
                btnNextPage.Visible = true;
                btnNextPage.Enabled = true;
            }

        }

        private void tmrRR_Tick(object sender, EventArgs e)
        {
            refreshInterval--;
            if (refreshInterval == 0)
            {
                RefreshDataOnScreen();
            }

            if (refreshButonInterval >= 0)
            {
                refreshButonInterval--;
                if (refreshButonInterval == 0)
                {
                    refreshButonInterval = -1;
                    btnRefresh.Enabled = true;
                }
            }

        }
        
        #endregion

        #region Misc Methods

        public void refreshWorkers()
        {
            freeWorkers = maxWorkers;
            int bussy = mC.GetUserBussyWorkersAll(username);
            freeWorkers -= bussy;

            // This should not happen, but we still prevent it
            if (freeWorkers < 0)
                freeWorkers = 0;

            lblWorkers.Text = "Workers:   " + freeWorkers + "/" + maxWorkers;
        }

        public string generateUserData()
        {
            string str = "userLevel:" + userLevel + "|workers:" + freeWorkers + "|maxWorkers:" + maxWorkers + "|";
            foreach(var rp in resursi)
            {
                if (rp.Value.IsUnlocked)
                    str += "res" + rp.Key + ":" + rp.Value.UserAmount + "|";
            }
            return str;
        }

        public void setupResourcesPanel()
        {
            pnlResources.Controls.Clear();
            // Add a textbox
            TextBox ress = new TextBox();
            ress.Location = new Point(0, 0);
            ress.Multiline = true;
            ress.ReadOnly = true;
            ress.TextAlign = HorizontalAlignment.Center;
            ress.Dock = DockStyle.Fill;
            ress.ScrollBars = ScrollBars.Vertical;
            pnlResources.Controls.Add(ress);

            // Take data from the resource pools and show it (if unlocked)
            List<ResourcePool> rps = new List<ResourcePool>();
            foreach (var kv in resursi)
            {
                if (kv.Value.IsUnlocked == true)
                    rps.Add(kv.Value);
            }
            rps.OrderBy(x => x.ID);
            ress.Text = "";
            for (int i = 0; i < rps.Count; i++)
            {
                ress.Text += "" + rps[i].Name + Environment.NewLine + rps[i].UserAmount + Environment.NewLine;
                ress.Text += Environment.NewLine;
            }
        }

        private void loadRPResourceStats()
        {
            // Load max and current resources for each unlocked RP
            string rpdata = mC.GetAllResources();
            resCapacity.Clear();
            string[] allres = rpdata.Split('|');
            foreach(string res in allres)
            {
                if (res != "")
                {
                    string[] icm = res.Split(':');
                    int id = Int32.Parse(icm[0]);
                    int cur = Int32.Parse(icm[1]);
                    int max = Int32.Parse(icm[2]);

                    int[] curMax = new int[2];
                    curMax[0] = cur;
                    curMax[1] = max;
                    resCapacity.Add(id, curMax);
                }
            }
        }

        public void UpdateRPSlots()
        {
            ResourcePool rp;
            ResourcePoolSlot[] slots = { rpSlot1, rpSlot2, rpSlot3 };
            int minID = curPage * slots.Count();
            for (int i = 0; i < slots.Count(); i++)
            {
                resursi.TryGetValue(minID + i, out rp);
                slots[i].RP = rp;
                slots[i].SetAndRefresh();
            }
        }
        
        public void RefreshDataOnScreen()
        {
            // Called every time the refreshInterval reaches zero
            loadUpgrades();
            setupResourcesPanel();
            loadRPResourceStats();
            UpdateRPSlots();
            parseUserData();
            refreshWorkers();

            refreshInterval = longRefreshInterval;
            lblUsername.Text = username + " (lvl " + userLevel + ")";
            lblWorkers.Text = "Workers:   " + freeWorkers + "/" + maxWorkers;
            Console.WriteLine("Game Screen Refreshed!");
        }

        #endregion

    }
}
