﻿namespace SideR.Forms
{
    partial class FormRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxMain = new System.Windows.Forms.GroupBox();
            this.lblPassAgain = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.tbxPasswordAgain = new System.Windows.Forms.TextBox();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.tbxUser = new System.Windows.Forms.TextBox();
            this.gbxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxMain
            // 
            this.gbxMain.Controls.Add(this.lblPassAgain);
            this.gbxMain.Controls.Add(this.lblPass);
            this.gbxMain.Controls.Add(this.lblUser);
            this.gbxMain.Controls.Add(this.btnRegister);
            this.gbxMain.Controls.Add(this.tbxPasswordAgain);
            this.gbxMain.Controls.Add(this.tbxPassword);
            this.gbxMain.Controls.Add(this.tbxUser);
            this.gbxMain.Location = new System.Drawing.Point(12, 8);
            this.gbxMain.Name = "gbxMain";
            this.gbxMain.Size = new System.Drawing.Size(308, 192);
            this.gbxMain.TabIndex = 0;
            this.gbxMain.TabStop = false;
            // 
            // lblPassAgain
            // 
            this.lblPassAgain.AutoSize = true;
            this.lblPassAgain.Location = new System.Drawing.Point(16, 108);
            this.lblPassAgain.Name = "lblPassAgain";
            this.lblPassAgain.Size = new System.Drawing.Size(82, 13);
            this.lblPassAgain.TabIndex = 6;
            this.lblPassAgain.Text = "Password again";
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(30, 72);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(53, 13);
            this.lblPass.TabIndex = 5;
            this.lblPass.Text = "Password";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(30, 32);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(55, 13);
            this.lblUser.TabIndex = 4;
            this.lblUser.Text = "Username";
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(117, 151);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(92, 24);
            this.btnRegister.TabIndex = 3;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // tbxPasswordAgain
            // 
            this.tbxPasswordAgain.Location = new System.Drawing.Point(117, 105);
            this.tbxPasswordAgain.MaxLength = 25;
            this.tbxPasswordAgain.Name = "tbxPasswordAgain";
            this.tbxPasswordAgain.Size = new System.Drawing.Size(158, 20);
            this.tbxPasswordAgain.TabIndex = 2;
            // 
            // tbxPassword
            // 
            this.tbxPassword.Location = new System.Drawing.Point(117, 69);
            this.tbxPassword.MaxLength = 25;
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.Size = new System.Drawing.Size(158, 20);
            this.tbxPassword.TabIndex = 1;
            // 
            // tbxUser
            // 
            this.tbxUser.Location = new System.Drawing.Point(117, 29);
            this.tbxUser.MaxLength = 30;
            this.tbxUser.Name = "tbxUser";
            this.tbxUser.Size = new System.Drawing.Size(158, 20);
            this.tbxUser.TabIndex = 0;
            // 
            // FormRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 211);
            this.Controls.Add(this.gbxMain);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(351, 249);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(351, 249);
            this.Name = "FormRegistration";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRegistration_FormClosing);
            this.gbxMain.ResumeLayout(false);
            this.gbxMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxMain;
        private System.Windows.Forms.TextBox tbxUser;
        private System.Windows.Forms.TextBox tbxPasswordAgain;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Label lblPassAgain;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUser;
    }
}