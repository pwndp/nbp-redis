﻿using Server.Entities;
using SideR.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR.Forms
{
    public partial class FormUpgrades : Form
    {
        public MojServer.MCClient mC = null;
        public FormGame GameForm { get; set; }
        List<Upgrade> _upgrades = null;
        int curPage = 0;
        int maxPage;

        public List<Upgrade> GetUpgradesList()
        {
            return _upgrades;
        }

        public List<Upgrade> Upgrades
        {
            get
            {
                return _upgrades;
            }
            set
            {
                _upgrades = value;
                PrepareUpgradeSlots();
            }
        }

        public FormUpgrades()
        {
            InitializeComponent();
            Upgrades = new List<Upgrade>();
        }

        private void PrepareUpgradeSlots()
        {
            maxPage = (int)Math.Ceiling(_upgrades.Count() / (float)5) - 1;

            btnPreviousPage.Visible = false;
            btnPreviousPage.Enabled = false;
            if (maxPage == curPage || maxPage<=0)
            {
                btnNextPage.Visible = true;
                btnNextPage.Enabled = true;
            }

            UpgradeSlot[] slots = { upSlot1, upSlot2, upSlot3, upSlot4, upSlot5 };
            for(int i = 0; i < slots.Length; i++)
            {
                slots[i].MyFormParent = this;
            }

            UpdateUpgradeSlots();
        }

        public void UpdateUpgradeSlots()
        {
            Upgrade ug;
            UpgradeSlot[] slots = { upSlot1, upSlot2, upSlot3, upSlot4, upSlot5 };
            int minID = curPage * slots.Count();
            for (int i = 0; i < slots.Count(); i++)
            {
                ug = null;
                if (minID + i < Upgrades.Count())
                    ug = Upgrades[minID + i];
                slots[i].Upgrade = ug;
            }
        }

        private void FormUpgrades_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            if (curPage > 0)
                curPage--;

            UpdateUpgradeSlots();

            if (curPage == 0)
            {
                btnPreviousPage.Visible = false;
                btnPreviousPage.Enabled = false;
            }

            if (curPage == maxPage)
            {
                btnNextPage.Visible = false;
                btnNextPage.Enabled = false;
            }
            else
            {
                btnNextPage.Visible = true;
                btnNextPage.Enabled = true;
            }
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            if (curPage < maxPage)
                curPage++;

            UpdateUpgradeSlots();

            if (curPage == maxPage)
            {
                btnNextPage.Visible = false;
                btnNextPage.Enabled = false;
            }

            if (curPage == 0)
            {
                btnPreviousPage.Visible = false;
                btnPreviousPage.Enabled = false;
            }
            else
            {
                btnPreviousPage.Visible = true;
                btnPreviousPage.Enabled = true;
            }
        }

    }
}
