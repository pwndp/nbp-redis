﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR.Forms
{
    public partial class FormPurchase : Form
    {
        #region Fields

        public MojServer.MCClient mC = null;
        public string ItemName { get; set; }
        public string ItemDesc { get; set; }
        public List<Price> prices { get; set; }
        public FormGame gameForm { get; set; }

        // Purchase fields
        public delegate bool PurchaseDelegate(Dictionary<string,byte[]> kv);
        public delegate Dictionary<string, byte[]> DataReturnDelegate();
        public PurchaseDelegate execPurchase;
        public DataReturnDelegate PurchaseData;

        #endregion

        public FormPurchase()
        {
            InitializeComponent();
            ItemName = "";
            ItemDesc = "";
            prices = new List<Price>();
        }
        
        private void FormKupovina_Load(object sender, EventArgs e)
        {
            //UpdateComponents();
        }
        
        private void btnNe_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            this.Close();
        }

        private void FormPurchase_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void btnDa_Click(object sender, EventArgs e)
        {
            if (!checkResources())
                return;

            bool suc = execPurchase(PurchaseData());
            if (suc) { 
                DialogResult = DialogResult.Yes;
                gameForm.RefreshDataOnScreen();
                MessageBox.Show("Purchase completed successfully!");
                this.Close();
            }
            else
            {
                DialogResult = DialogResult.No;
                MessageBox.Show("An error occured while trying to complete the purchase.");
            }
        }

        private bool checkResources()
        {
            // Prepare user unlocked resources
            List<ResourcePool> allRPs = new List<ResourcePool>();
            foreach(var kv in gameForm.resursi)
                allRPs.Add(kv.Value);

            // Check requirements
            int req = 0; // Number of unlocked requirements
            int meetReq = 0; // Number of fulfilled requirements
            foreach(Price p in prices)
            {
                bool found = false;
                for(int i=0;i< allRPs.Count && !found; i++)
                {
                    if (allRPs[i].IsUnlocked && allRPs[i].Name == p.Name)
                        req++;
                    if (allRPs[i].Name == p.Name && allRPs[i].UserAmount >= p.Amount)
                        meetReq++;
                }
            }
            
            // Check if required RPs are unlocked
            if (req != prices.Count)
            {
                string words = "resources are";
                if ((req + 1) == prices.Count())
                    words = "resource is";
                MessageBox.Show("Required "+words+" not unlocked.");
                return false;
            }

            // Check every price with users data
            if (meetReq != prices.Count)
            {
                MessageBox.Show("Not enough resources.");
                return false;
            }

            // Else return true
            return true;
        }

        public void UpdateComponents()
        {
            lblNazivItema.Text = ItemName;
            lblOpisItema.Text = ItemDesc;

            CenterToParent();
            lblNazivItema.Left = (this.ClientSize.Width - lblNazivItema.Width) / 2;
            lblOpisItema.Left = (this.ClientSize.Width - lblOpisItema.Width) / 2;
            txtItems.Text = "";
            for (int i = 0; i < prices.Count; i++)
            {
                txtItems.Text += "" + prices[i].Name + "\t\t " + prices[i].Amount + Environment.NewLine;
                txtItems.ScrollToCaret();
            }
        }
    }
}
