﻿namespace SideR.Forms
{
    partial class FormPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPitanje = new System.Windows.Forms.Label();
            this.btnDa = new System.Windows.Forms.Button();
            this.btnNe = new System.Windows.Forms.Button();
            this.lblNazivItema = new System.Windows.Forms.Label();
            this.lblOpisItema = new System.Windows.Forms.Label();
            this.txtItems = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelPitanje
            // 
            this.labelPitanje.AutoSize = true;
            this.labelPitanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPitanje.Location = new System.Drawing.Point(66, 302);
            this.labelPitanje.Name = "labelPitanje";
            this.labelPitanje.Size = new System.Drawing.Size(323, 20);
            this.labelPitanje.TabIndex = 4;
            this.labelPitanje.Text = "Are you sure you want to purchase the item?";
            this.labelPitanje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDa
            // 
            this.btnDa.Location = new System.Drawing.Point(139, 331);
            this.btnDa.Name = "btnDa";
            this.btnDa.Size = new System.Drawing.Size(75, 39);
            this.btnDa.TabIndex = 1;
            this.btnDa.Text = "Buy";
            this.btnDa.UseVisualStyleBackColor = true;
            this.btnDa.Click += new System.EventHandler(this.btnDa_Click);
            // 
            // btnNe
            // 
            this.btnNe.Location = new System.Drawing.Point(236, 331);
            this.btnNe.Name = "btnNe";
            this.btnNe.Size = new System.Drawing.Size(75, 39);
            this.btnNe.TabIndex = 2;
            this.btnNe.Text = "Cancel";
            this.btnNe.UseVisualStyleBackColor = true;
            this.btnNe.Click += new System.EventHandler(this.btnNe_Click);
            // 
            // lblNazivItema
            // 
            this.lblNazivItema.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNazivItema.Location = new System.Drawing.Point(12, 52);
            this.lblNazivItema.Name = "lblNazivItema";
            this.lblNazivItema.Size = new System.Drawing.Size(426, 45);
            this.lblNazivItema.TabIndex = 0;
            this.lblNazivItema.Text = "Naziv itema";
            this.lblNazivItema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOpisItema
            // 
            this.lblOpisItema.Location = new System.Drawing.Point(135, 97);
            this.lblOpisItema.Name = "lblOpisItema";
            this.lblOpisItema.Size = new System.Drawing.Size(176, 31);
            this.lblOpisItema.TabIndex = 3;
            this.lblOpisItema.Text = "Opis itema";
            this.lblOpisItema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtItems
            // 
            this.txtItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItems.Location = new System.Drawing.Point(104, 131);
            this.txtItems.MaxLength = 10000;
            this.txtItems.Multiline = true;
            this.txtItems.Name = "txtItems";
            this.txtItems.ReadOnly = true;
            this.txtItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtItems.Size = new System.Drawing.Size(262, 157);
            this.txtItems.TabIndex = 5;
            this.txtItems.TabStop = false;
            // 
            // FormPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 399);
            this.Controls.Add(this.txtItems);
            this.Controls.Add(this.lblOpisItema);
            this.Controls.Add(this.lblNazivItema);
            this.Controls.Add(this.btnNe);
            this.Controls.Add(this.btnDa);
            this.Controls.Add(this.labelPitanje);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(466, 437);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(466, 437);
            this.Name = "FormPurchase";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPurchase_FormClosing);
            this.Load += new System.EventHandler(this.FormKupovina_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPitanje;
        private System.Windows.Forms.Button btnDa;
        private System.Windows.Forms.Button btnNe;
        private System.Windows.Forms.Label lblNazivItema;
        private System.Windows.Forms.Label lblOpisItema;
        private System.Windows.Forms.TextBox txtItems;
    }
}