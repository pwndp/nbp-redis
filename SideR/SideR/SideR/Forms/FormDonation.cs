﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR.Forms
{
    public partial class FormDonation : Form
    {
        public FormGame gameForm { get; set; }
        public MojServer.MCClient mC = null;
        ResourcePool _rp = null;
        public ResourcePool ResPool
        {
            get
            {
                return _rp;
            }
            set
            {
                _rp = value;
                if (_rp != null)
                    this.Text = "Donate " + _rp.Name;
                else
                    this.Text = "Donation";
            }
        }
        
        public FormDonation()
        {
            InitializeComponent();
        }
        
        private void tbxAmount_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbxAmount.Text))
            {
                int val = Int32.Parse(tbxAmount.Text);
                if (val > ResPool.UserAmount)
                    val = ResPool.UserAmount;
                tbxAmount.Text = val.ToString();
            }
        }

        private void tbxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            // In case of successful donation
            int val = Int32.Parse(tbxAmount.Text);
            ResourcePool suc = gameForm.mC.MiningDonating(ResPool.ID, val, false);
            if (suc!=null && suc.IsUnlocked)
            {
                if (val > 0)
                {
                    gameForm.resursi[ResPool.ID].UserAmount -= val;
                    gameForm.setupResourcesPanel();
                    gameForm.mC.setUserData(gameForm.username, gameForm.generateUserData());
                }
                DialogResult = DialogResult.Yes;

                MessageBox.Show("Donation completed successfully!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Resource pool is already full.");
            }
        }
    }
}
