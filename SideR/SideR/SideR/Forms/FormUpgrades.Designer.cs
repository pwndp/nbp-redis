﻿namespace SideR.Forms
{
    partial class FormUpgrades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxContainer = new System.Windows.Forms.GroupBox();
            this.btnPreviousPage = new System.Windows.Forms.Button();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.upSlot5 = new SideR.Components.UpgradeSlot();
            this.upSlot4 = new SideR.Components.UpgradeSlot();
            this.upSlot3 = new SideR.Components.UpgradeSlot();
            this.upSlot2 = new SideR.Components.UpgradeSlot();
            this.upSlot1 = new SideR.Components.UpgradeSlot();
            this.gbxContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxContainer
            // 
            this.gbxContainer.Controls.Add(this.btnPreviousPage);
            this.gbxContainer.Controls.Add(this.btnNextPage);
            this.gbxContainer.Controls.Add(this.upSlot5);
            this.gbxContainer.Controls.Add(this.upSlot4);
            this.gbxContainer.Controls.Add(this.upSlot3);
            this.gbxContainer.Controls.Add(this.upSlot2);
            this.gbxContainer.Controls.Add(this.upSlot1);
            this.gbxContainer.Location = new System.Drawing.Point(12, 5);
            this.gbxContainer.Name = "gbxContainer";
            this.gbxContainer.Size = new System.Drawing.Size(382, 372);
            this.gbxContainer.TabIndex = 0;
            this.gbxContainer.TabStop = false;
            // 
            // btnPreviousPage
            // 
            this.btnPreviousPage.Location = new System.Drawing.Point(9, 177);
            this.btnPreviousPage.Name = "btnPreviousPage";
            this.btnPreviousPage.Size = new System.Drawing.Size(25, 25);
            this.btnPreviousPage.TabIndex = 6;
            this.btnPreviousPage.Text = "<-";
            this.btnPreviousPage.UseVisualStyleBackColor = true;
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            // 
            // btnNextPage
            // 
            this.btnNextPage.Location = new System.Drawing.Point(347, 177);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(25, 25);
            this.btnNextPage.TabIndex = 5;
            this.btnNextPage.Text = "->";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // upSlot5
            // 
            this.upSlot5.Location = new System.Drawing.Point(40, 295);
            this.upSlot5.Name = "upSlot5";
            this.upSlot5.Size = new System.Drawing.Size(301, 63);
            this.upSlot5.TabIndex = 4;
            this.upSlot5.Upgrade = null;
            // 
            // upSlot4
            // 
            this.upSlot4.Location = new System.Drawing.Point(40, 226);
            this.upSlot4.Name = "upSlot4";
            this.upSlot4.Size = new System.Drawing.Size(301, 63);
            this.upSlot4.TabIndex = 3;
            this.upSlot4.Upgrade = null;
            // 
            // upSlot3
            // 
            this.upSlot3.Location = new System.Drawing.Point(40, 157);
            this.upSlot3.Name = "upSlot3";
            this.upSlot3.Size = new System.Drawing.Size(301, 63);
            this.upSlot3.TabIndex = 2;
            this.upSlot3.Upgrade = null;
            // 
            // upSlot2
            // 
            this.upSlot2.Location = new System.Drawing.Point(40, 88);
            this.upSlot2.Name = "upSlot2";
            this.upSlot2.Size = new System.Drawing.Size(301, 63);
            this.upSlot2.TabIndex = 1;
            this.upSlot2.Upgrade = null;
            // 
            // upSlot1
            // 
            this.upSlot1.Location = new System.Drawing.Point(40, 19);
            this.upSlot1.Name = "upSlot1";
            this.upSlot1.Size = new System.Drawing.Size(301, 63);
            this.upSlot1.TabIndex = 0;
            this.upSlot1.Upgrade = null;
            // 
            // FormUpgrades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 387);
            this.Controls.Add(this.gbxContainer);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(422, 425);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(422, 425);
            this.Name = "FormUpgrades";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upgrades";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUpgrades_FormClosing);
            this.gbxContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxContainer;
        private Components.UpgradeSlot upSlot1;
        private Components.UpgradeSlot upSlot5;
        private Components.UpgradeSlot upSlot4;
        private Components.UpgradeSlot upSlot3;
        private Components.UpgradeSlot upSlot2;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.Button btnPreviousPage;
    }
}