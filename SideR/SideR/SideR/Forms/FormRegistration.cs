﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR.Forms
{
    public partial class FormRegistration : Form
    {

        public MojServer.MCClient mC = null;

        public FormRegistration()
        {
            InitializeComponent();
        }

        private bool checkInput()
        {
            bool isOkay = true;

            if (String.IsNullOrWhiteSpace(tbxUser.Text))
            {
                MessageBox.Show("Username field cannot be blank.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (String.IsNullOrWhiteSpace(tbxPassword.Text))
            {
                MessageBox.Show("Password field cannot be blank.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (String.IsNullOrWhiteSpace(tbxPasswordAgain.Text))
            {
                MessageBox.Show("Please confirm your password by typing it again.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            else if (tbxPassword.Text!=tbxPasswordAgain.Text)
            {
                MessageBox.Show("Entered passwords does not match.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isOkay = false;
            }
            return isOkay;
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (!checkInput())
                return;
            bool suc = false;
            suc = mC.registerUser(tbxUser.Text, tbxPassword.Text);
            if (suc)
            {
                const string newUserData = "userLevel:1|workers:0|maxWorkers:0|res0:0|";
                mC.setUserData(tbxUser.Text, newUserData);
                
                MessageBox.Show("Account created successfully!");
                this.Close();
            }
            else
                MessageBox.Show("Username already taken.");
        }

        private void FormRegistration_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
