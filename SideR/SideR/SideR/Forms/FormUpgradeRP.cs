﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SideR.Forms
{
    public partial class FormUpgradeRP : Form
    {
        ResourcePool _rp;
        public FormGame gameForm { get; set; }
        public ResourcePool RP
        {
            get
            {
                return _rp;
            }
            set
            {
                _rp = value;
                if (_rp != null)
                    SetUpStuff();
            }
        }
        int remaining;

        public FormUpgradeRP()
        {
            InitializeComponent();
            remaining = 0;
        }

        private void SetUpStuff()
        {
            this.Name = "Upgrade " + RP.Name + " Resources";

            if (gameForm!=null && gameForm.mC != null)
            {
                remaining = gameForm.mC.GetRemainingRPContributions(RP.ID);
                lblRemainingAmount.Text = "" + remaining + " " + RP.Name;
            }
        }

        private void tbxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!checkInput())
                return;
            int input = Int32.Parse(tbxAmount.Text);
            bool suc = gameForm.mC.SetRPContribution(RP.ID, input);
            if (suc)
            {
                gameForm.resursi[RP.ID].UserAmount -= input;
                gameForm.setupResourcesPanel();
                gameForm.mC.setUserData(gameForm.username, gameForm.generateUserData());
                remaining = gameForm.mC.GetRemainingRPContributions(RP.ID);
                lblRemainingAmount.Text = "" + remaining + " " + RP.Name;
                tbxAmount.Text = "";
                MessageBox.Show("Contribution was successful!");
            }
        }

        private bool checkInput()
        {
            if (String.IsNullOrWhiteSpace(tbxAmount.Text))
            {
                MessageBox.Show("Please enter an amount.");
                return false;
            }

            return true;
        }

        private void tbxAmount_TextChanged(object sender, EventArgs e)
        {
            // Now check the entered amount
            if (!String.IsNullOrWhiteSpace(tbxAmount.Text))
            {
                int input = Int32.Parse(tbxAmount.Text);
                int bigger = input;
                if (gameForm.resursi[RP.ID].UserAmount > remaining)
                    bigger = remaining;
                else
                    bigger = gameForm.resursi[RP.ID].UserAmount;

                if (input > bigger)
                    input = bigger;

                tbxAmount.Text = "" + input;
            }
        }
    }
}
