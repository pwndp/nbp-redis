﻿namespace SideR.Forms
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGame));
            this.gbxOptions = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnUpgrades = new System.Windows.Forms.Button();
            this.gbxRPs = new System.Windows.Forms.GroupBox();
            this.btnPreviousPage = new System.Windows.Forms.Button();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.gbxDock = new System.Windows.Forms.GroupBox();
            this.pnlResources = new System.Windows.Forms.Panel();
            this.lblResources = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblWorkers = new System.Windows.Forms.Label();
            this.tmrRR = new System.Windows.Forms.Timer(this.components);
            this.rpSlot3 = new SideR.Components.ResourcePoolSlot();
            this.rpSlot2 = new SideR.Components.ResourcePoolSlot();
            this.rpSlot1 = new SideR.Components.ResourcePoolSlot();
            this.gbxOptions.SuspendLayout();
            this.gbxRPs.SuspendLayout();
            this.gbxDock.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxOptions
            // 
            this.gbxOptions.BackColor = System.Drawing.SystemColors.Control;
            this.gbxOptions.Controls.Add(this.btnRefresh);
            this.gbxOptions.Controls.Add(this.btnUpgrades);
            this.gbxOptions.Location = new System.Drawing.Point(194, -9);
            this.gbxOptions.Name = "gbxOptions";
            this.gbxOptions.Size = new System.Drawing.Size(622, 61);
            this.gbxOptions.TabIndex = 4;
            this.gbxOptions.TabStop = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(578, 19);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(34, 32);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnUpgrades
            // 
            this.btnUpgrades.Location = new System.Drawing.Point(37, 19);
            this.btnUpgrades.Name = "btnUpgrades";
            this.btnUpgrades.Size = new System.Drawing.Size(101, 32);
            this.btnUpgrades.TabIndex = 1;
            this.btnUpgrades.Text = "Upgrades";
            this.btnUpgrades.UseVisualStyleBackColor = true;
            this.btnUpgrades.Click += new System.EventHandler(this.btnUpgrades_Click);
            // 
            // gbxRPs
            // 
            this.gbxRPs.Controls.Add(this.rpSlot3);
            this.gbxRPs.Controls.Add(this.rpSlot2);
            this.gbxRPs.Controls.Add(this.rpSlot1);
            this.gbxRPs.Controls.Add(this.btnPreviousPage);
            this.gbxRPs.Controls.Add(this.btnNextPage);
            this.gbxRPs.Location = new System.Drawing.Point(194, 58);
            this.gbxRPs.Name = "gbxRPs";
            this.gbxRPs.Size = new System.Drawing.Size(622, 346);
            this.gbxRPs.TabIndex = 5;
            this.gbxRPs.TabStop = false;
            // 
            // btnPreviousPage
            // 
            this.btnPreviousPage.Location = new System.Drawing.Point(4, 170);
            this.btnPreviousPage.Name = "btnPreviousPage";
            this.btnPreviousPage.Size = new System.Drawing.Size(25, 25);
            this.btnPreviousPage.TabIndex = 10;
            this.btnPreviousPage.Text = "<-";
            this.btnPreviousPage.UseVisualStyleBackColor = true;
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            // 
            // btnNextPage
            // 
            this.btnNextPage.Location = new System.Drawing.Point(587, 170);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(25, 25);
            this.btnNextPage.TabIndex = 9;
            this.btnNextPage.Text = "->";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // gbxDock
            // 
            this.gbxDock.Controls.Add(this.pnlResources);
            this.gbxDock.Controls.Add(this.lblResources);
            this.gbxDock.Location = new System.Drawing.Point(-6, 58);
            this.gbxDock.Name = "gbxDock";
            this.gbxDock.Size = new System.Drawing.Size(182, 344);
            this.gbxDock.TabIndex = 6;
            this.gbxDock.TabStop = false;
            // 
            // pnlResources
            // 
            this.pnlResources.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlResources.Location = new System.Drawing.Point(6, 38);
            this.pnlResources.Name = "pnlResources";
            this.pnlResources.Size = new System.Drawing.Size(174, 303);
            this.pnlResources.TabIndex = 10;
            // 
            // lblResources
            // 
            this.lblResources.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResources.Location = new System.Drawing.Point(9, 13);
            this.lblResources.Name = "lblResources";
            this.lblResources.Size = new System.Drawing.Size(158, 19);
            this.lblResources.TabIndex = 9;
            this.lblResources.Text = "Resources";
            this.lblResources.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUsername
            // 
            this.lblUsername.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(12, 7);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(176, 17);
            this.lblUsername.TabIndex = 7;
            this.lblUsername.Text = "Username (Lvl 999 )";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWorkers
            // 
            this.lblWorkers.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkers.Location = new System.Drawing.Point(12, 25);
            this.lblWorkers.Name = "lblWorkers";
            this.lblWorkers.Size = new System.Drawing.Size(176, 17);
            this.lblWorkers.TabIndex = 8;
            this.lblWorkers.Text = "Workers:   999/999";
            this.lblWorkers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrRR
            // 
            this.tmrRR.Interval = 1000;
            this.tmrRR.Tick += new System.EventHandler(this.tmrRR_Tick);
            // 
            // rpSlot3
            // 
            this.rpSlot3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rpSlot3.Location = new System.Drawing.Point(37, 235);
            this.rpSlot3.Name = "rpSlot3";
            this.rpSlot3.RP = null;
            this.rpSlot3.Size = new System.Drawing.Size(543, 102);
            this.rpSlot3.TabIndex = 13;
            // 
            // rpSlot2
            // 
            this.rpSlot2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rpSlot2.Location = new System.Drawing.Point(37, 127);
            this.rpSlot2.Name = "rpSlot2";
            this.rpSlot2.RP = null;
            this.rpSlot2.Size = new System.Drawing.Size(543, 102);
            this.rpSlot2.TabIndex = 12;
            // 
            // rpSlot1
            // 
            this.rpSlot1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rpSlot1.Location = new System.Drawing.Point(37, 19);
            this.rpSlot1.Name = "rpSlot1";
            this.rpSlot1.RP = null;
            this.rpSlot1.Size = new System.Drawing.Size(543, 102);
            this.rpSlot1.TabIndex = 11;
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 414);
            this.Controls.Add(this.lblWorkers);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.gbxDock);
            this.Controls.Add(this.gbxRPs);
            this.Controls.Add(this.gbxOptions);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(844, 452);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(844, 452);
            this.Name = "FormGame";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SideR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormGame_FormClosing);
            this.Load += new System.EventHandler(this.FormGame_Load);
            this.gbxOptions.ResumeLayout(false);
            this.gbxRPs.ResumeLayout(false);
            this.gbxDock.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxOptions;
        private System.Windows.Forms.Button btnUpgrades;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.GroupBox gbxRPs;
        private Components.ResourcePoolSlot rpSlot3;
        private Components.ResourcePoolSlot rpSlot2;
        private Components.ResourcePoolSlot rpSlot1;
        private System.Windows.Forms.Button btnPreviousPage;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.GroupBox gbxDock;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblWorkers;
        private System.Windows.Forms.Label lblResources;
        private System.Windows.Forms.Panel pnlResources;
        private System.Windows.Forms.Timer tmrRR;
    }
}