﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SideR.MojServer {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MojServer.IMC")]
    public interface IMC {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/TestFunc", ReplyAction="http://tempuri.org/IMC/TestFuncResponse")]
        void TestFunc();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/TestFunc", ReplyAction="http://tempuri.org/IMC/TestFuncResponse")]
        System.Threading.Tasks.Task TestFuncAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getItemPrice", ReplyAction="http://tempuri.org/IMC/getItemPriceResponse")]
        Server.Entities.ItemPrice getItemPrice(string upgradeName, int upgradeLevel);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getItemPrice", ReplyAction="http://tempuri.org/IMC/getItemPriceResponse")]
        System.Threading.Tasks.Task<Server.Entities.ItemPrice> getItemPriceAsync(string upgradeName, int upgradeLevel);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResourcePools", ReplyAction="http://tempuri.org/IMC/GetResourcePoolsResponse")]
        Server.Entities.ResourcePool[] GetResourcePools();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResourcePools", ReplyAction="http://tempuri.org/IMC/GetResourcePoolsResponse")]
        System.Threading.Tasks.Task<Server.Entities.ResourcePool[]> GetResourcePoolsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/MiningDonating", ReplyAction="http://tempuri.org/IMC/MiningDonatingResponse")]
        Server.Entities.ResourcePool MiningDonating(int idRP, int amount, bool mode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/MiningDonating", ReplyAction="http://tempuri.org/IMC/MiningDonatingResponse")]
        System.Threading.Tasks.Task<Server.Entities.ResourcePool> MiningDonatingAsync(int idRP, int amount, bool mode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetUserTimedData", ReplyAction="http://tempuri.org/IMC/SetUserTimedDataResponse")]
        bool SetUserTimedData(string username, int poolID, int workers, System.DateTime dt);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetUserTimedData", ReplyAction="http://tempuri.org/IMC/SetUserTimedDataResponse")]
        System.Threading.Tasks.Task<bool> SetUserTimedDataAsync(string username, int poolID, int workers, System.DateTime dt);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserTimedData", ReplyAction="http://tempuri.org/IMC/GetUserTimedDataResponse")]
        System.DateTime GetUserTimedData(string username, int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserTimedData", ReplyAction="http://tempuri.org/IMC/GetUserTimedDataResponse")]
        System.Threading.Tasks.Task<System.DateTime> GetUserTimedDataAsync(string username, int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetAllResources", ReplyAction="http://tempuri.org/IMC/GetAllResourcesResponse")]
        string GetAllResources();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetAllResources", ReplyAction="http://tempuri.org/IMC/GetAllResourcesResponse")]
        System.Threading.Tasks.Task<string> GetAllResourcesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResources", ReplyAction="http://tempuri.org/IMC/GetResourcesResponse")]
        string GetResources(int idRP);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResources", ReplyAction="http://tempuri.org/IMC/GetResourcesResponse")]
        System.Threading.Tasks.Task<string> GetResourcesAsync(int idRP);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResourcePoolLevel", ReplyAction="http://tempuri.org/IMC/GetResourcePoolLevelResponse")]
        int GetResourcePoolLevel(int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetResourcePoolLevel", ReplyAction="http://tempuri.org/IMC/GetResourcePoolLevelResponse")]
        System.Threading.Tasks.Task<int> GetResourcePoolLevelAsync(int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetRemainingRPContributions", ReplyAction="http://tempuri.org/IMC/GetRemainingRPContributionsResponse")]
        int GetRemainingRPContributions(int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetRemainingRPContributions", ReplyAction="http://tempuri.org/IMC/GetRemainingRPContributionsResponse")]
        System.Threading.Tasks.Task<int> GetRemainingRPContributionsAsync(int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetRPContribution", ReplyAction="http://tempuri.org/IMC/SetRPContributionResponse")]
        bool SetRPContribution(int poolID, int amnt);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetRPContribution", ReplyAction="http://tempuri.org/IMC/SetRPContributionResponse")]
        System.Threading.Tasks.Task<bool> SetRPContributionAsync(int poolID, int amnt);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetWorkerUpgradeData", ReplyAction="http://tempuri.org/IMC/SetWorkerUpgradeDataResponse")]
        bool SetWorkerUpgradeData(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/SetWorkerUpgradeData", ReplyAction="http://tempuri.org/IMC/SetWorkerUpgradeDataResponse")]
        System.Threading.Tasks.Task<bool> SetWorkerUpgradeDataAsync(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetWorkerUpgradeData", ReplyAction="http://tempuri.org/IMC/GetWorkerUpgradeDataResponse")]
        string GetWorkerUpgradeData(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetWorkerUpgradeData", ReplyAction="http://tempuri.org/IMC/GetWorkerUpgradeDataResponse")]
        System.Threading.Tasks.Task<string> GetWorkerUpgradeDataAsync(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getWorkerUpgradeDataValue", ReplyAction="http://tempuri.org/IMC/getWorkerUpgradeDataValueResponse")]
        int getWorkerUpgradeDataValue(string username, string upgradeName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getWorkerUpgradeDataValue", ReplyAction="http://tempuri.org/IMC/getWorkerUpgradeDataValueResponse")]
        System.Threading.Tasks.Task<int> getWorkerUpgradeDataValueAsync(string username, string upgradeName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getRPPerWorkerUpgrade", ReplyAction="http://tempuri.org/IMC/getRPPerWorkerUpgradeResponse")]
        int getRPPerWorkerUpgrade(string username, string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getRPPerWorkerUpgrade", ReplyAction="http://tempuri.org/IMC/getRPPerWorkerUpgradeResponse")]
        System.Threading.Tasks.Task<int> getRPPerWorkerUpgradeAsync(string username, string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserBussyWorkers", ReplyAction="http://tempuri.org/IMC/GetUserBussyWorkersResponse")]
        int GetUserBussyWorkers(string username, int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserBussyWorkers", ReplyAction="http://tempuri.org/IMC/GetUserBussyWorkersResponse")]
        System.Threading.Tasks.Task<int> GetUserBussyWorkersAsync(string username, int poolID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserBussyWorkersAll", ReplyAction="http://tempuri.org/IMC/GetUserBussyWorkersAllResponse")]
        int GetUserBussyWorkersAll(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/GetUserBussyWorkersAll", ReplyAction="http://tempuri.org/IMC/GetUserBussyWorkersAllResponse")]
        System.Threading.Tasks.Task<int> GetUserBussyWorkersAllAsync(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/registerUser", ReplyAction="http://tempuri.org/IMC/registerUserResponse")]
        bool registerUser(string username, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/registerUser", ReplyAction="http://tempuri.org/IMC/registerUserResponse")]
        System.Threading.Tasks.Task<bool> registerUserAsync(string username, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/checkUser", ReplyAction="http://tempuri.org/IMC/checkUserResponse")]
        bool checkUser(string username, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/checkUser", ReplyAction="http://tempuri.org/IMC/checkUserResponse")]
        System.Threading.Tasks.Task<bool> checkUserAsync(string username, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/setUserData", ReplyAction="http://tempuri.org/IMC/setUserDataResponse")]
        bool setUserData(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/setUserData", ReplyAction="http://tempuri.org/IMC/setUserDataResponse")]
        System.Threading.Tasks.Task<bool> setUserDataAsync(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserData", ReplyAction="http://tempuri.org/IMC/getUserDataResponse")]
        string getUserData(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserData", ReplyAction="http://tempuri.org/IMC/getUserDataResponse")]
        System.Threading.Tasks.Task<string> getUserDataAsync(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/setUserUpgrades", ReplyAction="http://tempuri.org/IMC/setUserUpgradesResponse")]
        bool setUserUpgrades(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/setUserUpgrades", ReplyAction="http://tempuri.org/IMC/setUserUpgradesResponse")]
        System.Threading.Tasks.Task<bool> setUserUpgradesAsync(string username, string data);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserUpgrades", ReplyAction="http://tempuri.org/IMC/getUserUpgradesResponse")]
        Server.Entities.Upgrade[] getUserUpgrades(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserUpgrades", ReplyAction="http://tempuri.org/IMC/getUserUpgradesResponse")]
        System.Threading.Tasks.Task<Server.Entities.Upgrade[]> getUserUpgradesAsync(string username);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserUpgradeLevel", ReplyAction="http://tempuri.org/IMC/getUserUpgradeLevelResponse")]
        int getUserUpgradeLevel(string username, string upgradeName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMC/getUserUpgradeLevel", ReplyAction="http://tempuri.org/IMC/getUserUpgradeLevelResponse")]
        System.Threading.Tasks.Task<int> getUserUpgradeLevelAsync(string username, string upgradeName);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMCChannel : SideR.MojServer.IMC, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MCClient : System.ServiceModel.ClientBase<SideR.MojServer.IMC>, SideR.MojServer.IMC {
        
        public MCClient() {
        }
        
        public MCClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MCClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MCClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MCClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void TestFunc() {
            base.Channel.TestFunc();
        }
        
        public System.Threading.Tasks.Task TestFuncAsync() {
            return base.Channel.TestFuncAsync();
        }
        
        public Server.Entities.ItemPrice getItemPrice(string upgradeName, int upgradeLevel) {
            return base.Channel.getItemPrice(upgradeName, upgradeLevel);
        }
        
        public System.Threading.Tasks.Task<Server.Entities.ItemPrice> getItemPriceAsync(string upgradeName, int upgradeLevel) {
            return base.Channel.getItemPriceAsync(upgradeName, upgradeLevel);
        }
        
        public Server.Entities.ResourcePool[] GetResourcePools() {
            return base.Channel.GetResourcePools();
        }
        
        public System.Threading.Tasks.Task<Server.Entities.ResourcePool[]> GetResourcePoolsAsync() {
            return base.Channel.GetResourcePoolsAsync();
        }
        
        public Server.Entities.ResourcePool MiningDonating(int idRP, int amount, bool mode) {
            return base.Channel.MiningDonating(idRP, amount, mode);
        }
        
        public System.Threading.Tasks.Task<Server.Entities.ResourcePool> MiningDonatingAsync(int idRP, int amount, bool mode) {
            return base.Channel.MiningDonatingAsync(idRP, amount, mode);
        }
        
        public bool SetUserTimedData(string username, int poolID, int workers, System.DateTime dt) {
            return base.Channel.SetUserTimedData(username, poolID, workers, dt);
        }
        
        public System.Threading.Tasks.Task<bool> SetUserTimedDataAsync(string username, int poolID, int workers, System.DateTime dt) {
            return base.Channel.SetUserTimedDataAsync(username, poolID, workers, dt);
        }
        
        public System.DateTime GetUserTimedData(string username, int poolID) {
            return base.Channel.GetUserTimedData(username, poolID);
        }
        
        public System.Threading.Tasks.Task<System.DateTime> GetUserTimedDataAsync(string username, int poolID) {
            return base.Channel.GetUserTimedDataAsync(username, poolID);
        }
        
        public string GetAllResources() {
            return base.Channel.GetAllResources();
        }
        
        public System.Threading.Tasks.Task<string> GetAllResourcesAsync() {
            return base.Channel.GetAllResourcesAsync();
        }
        
        public string GetResources(int idRP) {
            return base.Channel.GetResources(idRP);
        }
        
        public System.Threading.Tasks.Task<string> GetResourcesAsync(int idRP) {
            return base.Channel.GetResourcesAsync(idRP);
        }
        
        public int GetResourcePoolLevel(int poolID) {
            return base.Channel.GetResourcePoolLevel(poolID);
        }
        
        public System.Threading.Tasks.Task<int> GetResourcePoolLevelAsync(int poolID) {
            return base.Channel.GetResourcePoolLevelAsync(poolID);
        }
        
        public int GetRemainingRPContributions(int poolID) {
            return base.Channel.GetRemainingRPContributions(poolID);
        }
        
        public System.Threading.Tasks.Task<int> GetRemainingRPContributionsAsync(int poolID) {
            return base.Channel.GetRemainingRPContributionsAsync(poolID);
        }
        
        public bool SetRPContribution(int poolID, int amnt) {
            return base.Channel.SetRPContribution(poolID, amnt);
        }
        
        public System.Threading.Tasks.Task<bool> SetRPContributionAsync(int poolID, int amnt) {
            return base.Channel.SetRPContributionAsync(poolID, amnt);
        }
        
        public bool SetWorkerUpgradeData(string username, string data) {
            return base.Channel.SetWorkerUpgradeData(username, data);
        }
        
        public System.Threading.Tasks.Task<bool> SetWorkerUpgradeDataAsync(string username, string data) {
            return base.Channel.SetWorkerUpgradeDataAsync(username, data);
        }
        
        public string GetWorkerUpgradeData(string username) {
            return base.Channel.GetWorkerUpgradeData(username);
        }
        
        public System.Threading.Tasks.Task<string> GetWorkerUpgradeDataAsync(string username) {
            return base.Channel.GetWorkerUpgradeDataAsync(username);
        }
        
        public int getWorkerUpgradeDataValue(string username, string upgradeName) {
            return base.Channel.getWorkerUpgradeDataValue(username, upgradeName);
        }
        
        public System.Threading.Tasks.Task<int> getWorkerUpgradeDataValueAsync(string username, string upgradeName) {
            return base.Channel.getWorkerUpgradeDataValueAsync(username, upgradeName);
        }
        
        public int getRPPerWorkerUpgrade(string username, string name) {
            return base.Channel.getRPPerWorkerUpgrade(username, name);
        }
        
        public System.Threading.Tasks.Task<int> getRPPerWorkerUpgradeAsync(string username, string name) {
            return base.Channel.getRPPerWorkerUpgradeAsync(username, name);
        }
        
        public int GetUserBussyWorkers(string username, int poolID) {
            return base.Channel.GetUserBussyWorkers(username, poolID);
        }
        
        public System.Threading.Tasks.Task<int> GetUserBussyWorkersAsync(string username, int poolID) {
            return base.Channel.GetUserBussyWorkersAsync(username, poolID);
        }
        
        public int GetUserBussyWorkersAll(string username) {
            return base.Channel.GetUserBussyWorkersAll(username);
        }
        
        public System.Threading.Tasks.Task<int> GetUserBussyWorkersAllAsync(string username) {
            return base.Channel.GetUserBussyWorkersAllAsync(username);
        }
        
        public bool registerUser(string username, string password) {
            return base.Channel.registerUser(username, password);
        }
        
        public System.Threading.Tasks.Task<bool> registerUserAsync(string username, string password) {
            return base.Channel.registerUserAsync(username, password);
        }
        
        public bool checkUser(string username, string password) {
            return base.Channel.checkUser(username, password);
        }
        
        public System.Threading.Tasks.Task<bool> checkUserAsync(string username, string password) {
            return base.Channel.checkUserAsync(username, password);
        }
        
        public bool setUserData(string username, string data) {
            return base.Channel.setUserData(username, data);
        }
        
        public System.Threading.Tasks.Task<bool> setUserDataAsync(string username, string data) {
            return base.Channel.setUserDataAsync(username, data);
        }
        
        public string getUserData(string username) {
            return base.Channel.getUserData(username);
        }
        
        public System.Threading.Tasks.Task<string> getUserDataAsync(string username) {
            return base.Channel.getUserDataAsync(username);
        }
        
        public bool setUserUpgrades(string username, string data) {
            return base.Channel.setUserUpgrades(username, data);
        }
        
        public System.Threading.Tasks.Task<bool> setUserUpgradesAsync(string username, string data) {
            return base.Channel.setUserUpgradesAsync(username, data);
        }
        
        public Server.Entities.Upgrade[] getUserUpgrades(string username) {
            return base.Channel.getUserUpgrades(username);
        }
        
        public System.Threading.Tasks.Task<Server.Entities.Upgrade[]> getUserUpgradesAsync(string username) {
            return base.Channel.getUserUpgradesAsync(username);
        }
        
        public int getUserUpgradeLevel(string username, string upgradeName) {
            return base.Channel.getUserUpgradeLevel(username, upgradeName);
        }
        
        public System.Threading.Tasks.Task<int> getUserUpgradeLevelAsync(string username, string upgradeName) {
            return base.Channel.getUserUpgradeLevelAsync(username, upgradeName);
        }
    }
}
