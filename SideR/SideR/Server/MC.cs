﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Server.Entities;
using Server.MiscStuff;

namespace Server
{
    // MC - Main class
    public class MC : IMC
    {
        // Preset upgrade prices (located in the Requrements\upgradesData.xml file)
        private Dictionary<string, UpgradeData> upgradePrices = UpgradesData.Instance.getData();

        public void TestFunc()
        {

        }

        #region Getter functions

        public ItemPrice getItemPrice(string upgradeName, int upgradeLevel)
        {
            if (upgradePrices.ContainsKey(upgradeName))
            {
                upgradePrices.TryGetValue(upgradeName, out UpgradeData ud);
                if (ud != null && ud.prices.ContainsKey(upgradeLevel))
                {
                    ud.prices.TryGetValue(upgradeLevel, out ItemPrice ip);
                    return ip;
                }
            }
            return null;
        }
        public List<ResourcePool> GetResourcePools()
        {
            ResourcePoolsData rpd = new ResourcePoolsData();
            return rpd.GetData();
        }
        private List<Upgrade> getAllLevel1Upgrades()
        {
            List<Upgrade> upgrades = new List<Upgrade>();

            foreach (var item in upgradePrices)
            {
                Upgrade up = new Upgrade(item.Value.prices[1].GetPrices());
                up.Level = 1;
                up.MaxLevel = item.Value.maxLevel;
                up.Name = item.Key;
                upgrades.Add(up);
            }

            return upgrades;
        }

        #endregion

        #region Account functions

        // Adds a new user (returns true). If there already is one, returns false
        public bool registerUser(string username, string password)
        {
            RedisDataLayer red = new RedisDataLayer();
            return red.AddUser(username, password);
        }

        // Checks if the username and password are correct
        public bool checkUser(string username, string password)
        {
            RedisDataLayer red = new RedisDataLayer();
            return red.CheckUser(username, password);
        }

        // Sets the data for the user
        public bool setUserData(string username, string data)
        {
            // The data is in format:
            // "level:0|workers:0|maxWorkers:0"
            RedisDataLayer rd = new RedisDataLayer();
            return rd.SetUserData(username, data);
        }

        // Gets the user data
        public string getUserData(string username)
        {
            RedisDataLayer rd = new RedisDataLayer();
            return rd.GetUserData(username);
        }

        // Sets the upgrades for a user
        public bool setUserUpgrades(string username, string data)
        {
            // Data is in the format below:
            // "upgrade1:level1|upgrade2:level2|upgrade3:level3"
            // Take in mind that the data can be empty string
            RedisDataLayer rd = new RedisDataLayer();
            return rd.SetUserUpgrades(username, data);
        }

        // Gets the upgrades for a user
        public List<Upgrade> getUserUpgrades(string username)
        {
            RedisDataLayer rd = new RedisDataLayer();
            List<Upgrade> result = getAllLevel1Upgrades();

            string data = rd.GetUserUpgrades(username);
            string[] upgs = data.Split('|');

            // Check if user has any data
            if (!(upgs.Length == 1 && upgs[0] == ""))
            {
                foreach (string up in upgs)
                {
                    if (up != "")
                    {
                        string[] kv = up.Split(':');
                        // Now find and apply the changes
                        bool found = false;
                        for (int i = 0; i < result.Count() && !found; i++)
                        {
                            if (result[i].Name == kv[0])
                            {
                                int lvl = Int32.Parse(kv[1]);
                                result[i].Level = lvl;
                                result[i].SetPrices(null);
                                ItemPrice ip = getItemPrice(kv[0], lvl);
                                if (ip != null)
                                    result[i].SetPrices(ip.GetPrices()); // Ovde je greska
                                found = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        // Mining or Donating resources
        public ResourcePool MiningDonating(int idRP, int amount, bool mode)    //storing from RP to user inventory
        {
            // mode = true: Mining
            // mode = false: Donating
            int fetchedLevel = -1;    // fetched from string by using getRPAmount(idRP)
            int fetchedAmount = -1;   // fetched from string by using getRPAmount(idRP)
            int maxAmount = -1;   // per level. Formula: mA=fetchedLevel*MinValueOfRP
            string containter;  // used for getting raw string from database
            string[] s1;    // used for separating level and amount

            ResourcePool rp = new ResourcePool();
            RedisDataLayer rdl = new RedisDataLayer();
            containter = rdl.getRPAmount(idRP); // level:1|amount:100
            s1 = containter.Split('|'); // s1[0] = "level:1", s1[1] = "amount:100"

            List<ResourcePool> _rps = GetResourcePools();
            foreach (ResourcePool item in _rps)
            {
                if (item.ID == idRP)
                {
                    rp.ID = idRP;
                    rp.Name = item.Name;
                    rp.IsUnlocked = true;
                    rp.PerWorkerAmount = item.PerWorkerAmount;
                    break;
                }
            }

            foreach (var s in s1)
            {
                string[] checkString = s.Split(':');

                if (checkString[0] == "level")
                {
                    fetchedLevel = Int32.Parse(checkString[1]);
                }

                if (checkString[0] == "amount")
                {
                    fetchedAmount = Int32.Parse(checkString[1]);
                }
            }
            maxAmount = fetchedLevel * rdl.MinValueOfRP;

            if (mode == true)    // Mining
            {
                int mined = amount;
                if (fetchedLevel > 1)
                {
                    if (fetchedAmount - amount <= 0)
                    {
                        fetchedLevel--;
                        fetchedAmount = fetchedLevel * rdl.MinValueOfRP;
                        mined = fetchedAmount;
                    }
                    else
                    {
                        fetchedAmount -= amount;
                    }
                }
                else
                {
                    if (fetchedAmount - amount <= 0)
                    {
                        fetchedLevel = 1;
                        fetchedAmount = fetchedLevel * rdl.MinValueOfRP;
                        mined = fetchedAmount;
                    }
                    else
                    {
                        fetchedAmount -= amount;
                    }
                }

                rp.UserAmount = mined;
            }
            else if (mode == false)  // Donating
            {
                if (fetchedAmount == maxAmount)
                {
                    rp.IsUnlocked = false;
                }
                else if (fetchedAmount + amount > maxAmount)
                {
                    fetchedAmount = maxAmount;  // Ako korisnik hoce da donira vise nego sto je kapacitet, odbacice se visak
                }
                else
                {
                    fetchedAmount += amount;
                }

            }

            rdl.setRPAmount(idRP, fetchedLevel, fetchedAmount);
            return rp;
        }

        // Sets a timer for user mined resource pool
        public bool SetUserTimedData(string username, int poolID, int workerNum, DateTime dt)
        {
            bool OkayData = false;
            RedisDataLayer rd = new RedisDataLayer();
            int secs = 0;
            string data = "" + workerNum + "|";
            if (dt != null)
            {
                data += dt.ToString("yyyy-MM-dd HH-mm-ss");
                TimeSpan r = (DateTime.Now - dt);
                secs = (int)r.TotalSeconds + 1; // Transform the cut-off miliseconds into a second
                OkayData = true;
            }

            rd.setUserTimedData(username, poolID, data, secs);
            return OkayData;
        }

        // Gets the time when the resource pool can be mined again
        public DateTime GetUserTimedData(string username, int poolID)
        {
            RedisDataLayer rd = new RedisDataLayer();
            string raw = rd.getUserTimedData(username, poolID);

            if (raw == "")
                return DateTime.Now;

            string[] spl = raw.Split('|');
            int workers = Int32.Parse(spl[0]);
            string datetimedata = spl[1];

            DateTime ndt = DateTime.ParseExact(datetimedata, "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);
            return ndt;
        }

        // Gets the number of assigned workers on a resource
        public int GetUserBussyWorkers(string username, int poolID)
        {
            RedisDataLayer rd = new RedisDataLayer();
            string raw = rd.getUserTimedData(username, poolID);

            if (raw == "")
                return 0;

            string[] spl = raw.Split('|');
            int workers = Int32.Parse(spl[0]);
            DateTime ndt = DateTime.ParseExact(spl[1], "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);

            if (ndt < DateTime.Now)
                return 0;
            return workers;
        }

        // Gets the number of all assigned workers on any resource
        public int GetUserBussyWorkersAll(string username)
        {
            int rez = 0;
            List<ResourcePool> rps = GetResourcePools();
            foreach (ResourcePool rp in rps)
                rez += GetUserBussyWorkers(username, rp.ID);
            return rez;
        }

        // Returns all resource pool ids, current resource values and max resource values
        public string GetAllResources()
        {
            // result format = idRes:currAmntRes:maxAmntRes|idRes:currAmntRes:maxAmntRes ...
            string result = "";
            int level = -1;
            int amount = -1;
            int maxRes = -1;
            string[] s1;
            string containter;
            RedisDataLayer rdl = new RedisDataLayer();
            List<ResourcePool> listaRP = listaRP = GetResourcePools();

            for (int i = 0; i < listaRP.Count; i++)
            {
                containter = rdl.getRPAmount(listaRP[i].ID); // level:1|amount:100
                s1 = containter.Split('|'); // s1[0] = "level:1", s1[1] = "amount:100"

                foreach (var s in s1)
                {
                    string[] checkString = s.Split(':');

                    if (checkString[0] == "level")
                    {
                        level = Int32.Parse(checkString[1]);
                    }

                    if (checkString[0] == "amount")
                    {
                        amount = Int32.Parse(checkString[1]);
                    }
                }
                maxRes = level * rdl.MinValueOfRP;
                if (i < listaRP.Count - 1)
                {
                    result += listaRP[i].ID + ":" + amount + ":" + maxRes + "|";
                }
                else
                {
                    result += listaRP[i].ID + ":" + amount + ":" + maxRes;
                }

            }
            return result;
        }

        // Returns the resource pool id, current resource value and max resource value of the requested resource pool
        public string GetResources(int idRP)
        {
            // result format = idRes:currAmntRes:maxAmntRes
            string result = "";
            int level = -1;
            int amount = -1;
            int maxRes = -1;
            string[] s1;
            string containter;
            RedisDataLayer rdl = new RedisDataLayer();
            List<ResourcePool> listaRP = new List<ResourcePool>();

            containter = rdl.getRPAmount(listaRP[idRP].ID); // level:1|amount:100
            s1 = containter.Split('|'); // s1[0] = "level:1", s1[1] = "amount:100"

            foreach (var s in s1)
            {
                string[] checkString = s.Split(':');

                if (checkString[0] == "level")
                {
                    level = Int32.Parse(checkString[1]);
                }

                if (checkString[0] == "amount")
                {
                    amount = Int32.Parse(checkString[1]);
                }
            }
            maxRes = level * rdl.MinValueOfRP;

            result += listaRP[idRP].ID + ":" + amount + ":" + maxRes;

            return result;
        }

        // Returns the current level of the requested resource pool
        public int GetResourcePoolLevel(int poolID)
        {
            RedisDataLayer rd = new RedisDataLayer();
            return rd.getRPLevel(poolID);
        }

        // Returns the current level of the requested upgrade for the specified level (if found)
        public int getUserUpgradeLevel(string username, string upgradeName)
        {
            RedisDataLayer rd = new RedisDataLayer();
            int res = 1;
            string up = rd.GetUserUpgrades(username);
            if (up != "")
            {
                string[] upgs = up.Split('|');
                foreach (string u in upgs)
                {
                    if (u != "")
                    {
                        string[] sp = u.Split(':');
                        if (sp[0].Contains(upgradeName))
                            res = Int32.Parse(sp[1]);
                    }
                }
            }
            return res;
        }

        // Returns the remaining amount for the upgrade of a resource pool
        public int GetRemainingRPContributions(int poolID)
        {
            RedisDataLayer rd = new RedisDataLayer();
            int contrib = rd.getRPContribution(poolID);
            int maxContrib = rd.getRPLevel(poolID) * rd.MinContribValue;
            return maxContrib - contrib;
        }

        // Sets the amount of the contribution for a given resource pool (and advances to the next level if enough)
        public bool SetRPContribution(int poolID, int amnt)
        {
            RedisDataLayer rd = new RedisDataLayer();
            int rpLevel = rd.getRPLevel(poolID);
            int maxCap = rpLevel * rd.MinContribValue;
            int currentVal = rd.getRPContribution(poolID);
            bool response = true;
            if (currentVal + amnt >= maxCap)
            {
                // Advance to the next level
                int nlvl = rpLevel + 1;
                bool s1 = rd.setRPAmount(poolID, nlvl, nlvl * rd.MinValueOfRP);
                bool s2 = rd.setRPContribution(poolID, 0);
                response = s1 & s2;
            }
            else
                response = rd.setRPContribution(poolID, currentVal + amnt);
            return response;
        }
        
        // Sets the worker upgrades data
        public bool SetWorkerUpgradeData(string username, string data)
        {
            RedisDataLayer rd = new RedisDataLayer();
            return rd.setWorkerUpgradeData(username, data);
        }
        
        // Returns the whole worker upgrade data
        public string GetWorkerUpgradeData(string username)
        {
            RedisDataLayer rd = new RedisDataLayer();
            return rd.getWorkerUpgradeData(username);
        }

        // Returns the user worker value of a specific upgrade
        public int getWorkerUpgradeDataValue(string username, string upgradeName)
        {
            RedisDataLayer rd = new RedisDataLayer();
            string raw = rd.getWorkerUpgradeData(username);
            int retval = 0;

            if (!String.IsNullOrWhiteSpace(raw))
            {
                string[] msplit = raw.Split('|');
                foreach(string stsp in msplit)
                {
                    if (stsp != "")
                    {
                        string[] kv = stsp.Split(':');
                        if (kv[0] == upgradeName)
                            retval = Int32.Parse(kv[1]);
                        break;
                    }
                }
            }

            return retval;
        }

        // Returns the amount per worker level of a resource pool for a specified user
        public int getRPPerWorkerUpgrade(string username, string upgradeName)
        {
            RedisDataLayer rd = new RedisDataLayer();
            string raw = rd.getWorkerUpgradeData(username);
            int retval = 0;

            if (!String.IsNullOrWhiteSpace(raw))
            {
                string[] msplit = raw.Split('|');
                foreach (string stsp in msplit)
                {
                    if (stsp != "")
                    {
                        string[] kv = stsp.Split(':');
                        string nam = kv[0].ToLower();
                        if (nam.Contains(upgradeName.ToLower()) && (nam.Contains("worker") || nam.Contains("workers")))
                            retval = Int32.Parse(kv[1]);
                        break;
                    }
                }
            }

            return retval;
        }

        

        #endregion

    }

}
