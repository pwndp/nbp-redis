﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Interfaces
{
    // Interface that forces an parameterless constructor, and implements the OnLoad function
    public interface IXMLDataWorker<T> where T:IXMLDataWorker<T>, new()
    {
        void OnLoad(object xmlSerializableFile);
    }
}
