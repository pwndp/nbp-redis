﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Entities
{
    // Represents a level of an upgrade along with its price.
    public class Upgrade
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int MaxLevel { get; set; }
        private List<Price> Prices;

        public Upgrade() {
            Name = "";
            Level = 0;
            MaxLevel = 0;
            Prices = new List<Price>();
        }
        
        public Upgrade(List<Price> prices, string name="", int lvl=0)
        {
            Prices = prices;
            Name = name;
            Level = lvl;
        }

        public void Add(Price p)
        {
            Prices.Add(p);
        }

        public int PriceNum()
        {
            return Prices.Count;
        }

        public Price this[int id]
        {
            get
            {
                if (id < 0 || id >= PriceNum())
                    return null;
                return Prices[id];
            }
        }

        public List<Price> GetPrices()
        {
            return Prices;
        }
        
        public void SetPrices(List<Price> pr)
        {
            Prices = pr;
        }
    }
}
