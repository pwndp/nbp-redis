﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Server.Entities;

namespace Server.Entities
{
    // Price that has a currency and amount. Used for serialization of data, but can also be used in other scenarios.
    [Serializable]
    public class Price
    {
        [XmlAttribute("price_name")]
        public string Name { get; set; }

        [XmlAttribute("price_amount")]
        public int Amount { get; set; }
    }
}
