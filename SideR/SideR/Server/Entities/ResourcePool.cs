﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Server.Entities
{
    [Serializable]
    public class ResourcePool
    {
        public ResourcePool()
        {
            Name = "";
            ID = 0;
            IsUnlocked = false;
            PerWorkerAmount = 0;
            Price = new List<Price>();
            UserAmount = 0;
        }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("id")]
        public int ID { get; set; }

        [XmlAttribute("defaultIsUnlocked")]
        public bool IsUnlocked { get; set; }

        [XmlElement("PerWorkerAmount")]
        public int PerWorkerAmount { get; set; }

        [XmlArray("UnlockPrice")]
        [XmlArrayItem("Price")]
        public List<Price> Price { get; set; }
        
        [XmlIgnore]
        public int UserAmount { get; set; }
        
    }
}
