﻿using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Server.Entities
{
    /// <summary>
    /// Saves, Loads and Handles data from XML files. When inherited, the new class should override the OnLoad function and 
    /// implement it in its own way. Also, before saving or loading files, file name should be set to something new.
    /// Every property that is intended to be saved, you have appropriate xml attribute tag: 
    /// XmlAttribute, XmlElement, XmlArray, XmlArrayItem, ...
    /// </summary>
    public class XMLDataWorker:IXMLDataWorker<XMLDataWorker>
    {
        protected string fileName = "data.xml";
        public XMLDataWorker() { }

        public bool Save()
        {
            try
            {
                string mFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
                Type t = this.GetType();
                var xmlSerializer = new XmlSerializer(t);
                using (var fileWriter = new FileStream(mFilePath, FileMode.Create))
                    xmlSerializer.Serialize(fileWriter, this);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool Load()
        {
            try
            {
                var xmlSer = getXmlSerializerAsObject();
                OnLoad(xmlSer);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void SetFileName(string name)
        {
            fileName = name;
        }

        private object getXmlSerializerAsObject()
        {
            string mFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            if (!File.Exists(mFilePath)) return null;
            var xmlSerializer = new XmlSerializer(GetType());
            using (var fileReader = XmlReader.Create(mFilePath))
            {
                return xmlSerializer.Deserialize(fileReader);
            }
        }
        
        /// <summary>
        /// This function is called after the data has been loaded from the xml file.
        /// <paramref name="xmlSerializer"/> The parameter should be cast into the current class so it can be used to access its values.
        /// </summary>
        public virtual void OnLoad(object xmlSerializer)
        {
            // Predefine this function after inherited
        }
        
    }
}
