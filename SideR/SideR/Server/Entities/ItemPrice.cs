﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Server.Entities
{
    // ItemPrice represents the list of prices of an upgrade's specific level.
    [Serializable]
    public class ItemPrice
    {
        [XmlArray("Prices")]
        [XmlArrayItem("Price")]
        private List<Price> prices;

        public ItemPrice()
        {
            prices = new List<Price>();
        }

        public ItemPrice(List<Price> ls)
        {
            prices = ls;
        }

        public int NumberOfPrices()
        {
            return prices.Count();
        }

        public void Add(Price p)
        {
            prices.Add(p);
        }

        public Price this[int id]
        {
            get
            {
                if (id < 0 || id >= NumberOfPrices())
                    return null;
                return prices[id];
            }

        }

        public List<Price> GetPrices()
        {
            return prices;
        }

    }
}
