﻿using ServiceStack.Redis;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class RedisDataLayer
    {

        #region Fields

        readonly RedisClient redis = new RedisClient(Config.SingleHost);
        public const int minValueOfRP = 200;
        public const int minContribValue = 1000;

        #endregion

        #region Properties and Constructors

        public int MinValueOfRP
        {
            get { return minValueOfRP; }
        }

        public int MinContribValue
        {
            get
            {
                return minContribValue;
            }
        }

        public RedisDataLayer()
        {
        }

        #endregion

        #region Function

        public bool AddUser(string username, string password)
        {
            bool succ = false;
            string entry = "user:" + username + ":pass";
            if (redis.Exists(entry) == 0)
            {
                redis.Set(entry, password);
                redis.PushItemToList("global:users", entry);
                succ = true;
            }
            return succ;
        }

        public bool CheckUser(string username, string password)
        {
            bool succ = false;
            string entry = "user:" + username + ":pass";
            if (redis.Exists(entry) != 0)
            {
                string pass = redis.Get<string>(entry);
                if (pass==password)
                    succ = true;
            }
            return succ;
        }

        public bool SetUserData(string username, string data)
        {
            string entry = "user:" + username + ":data";
            bool succ = redis.Set(entry, data);
            return succ;
        }

        public string GetUserData(string username)
        {
            string res = "";
            string entry = "user:" + username + ":data";
            if (redis.Exists(entry) != 0)
            {
                res = redis.Get<string>(entry);
            }
            return res;
        }

        public bool SetUserUpgrades(string username, string data)
        {
            string entry = "user:" + username + ":upgrades";
            bool succ = redis.Set(entry, data);
            return succ;
        }

        public string GetUserUpgrades(string username)
        {
            string res = "";
            string entry = "user:" + username + ":upgrades";
            if (redis.Exists(entry) == 0)
                redis.Set(entry, "");
            res = redis.Get<string>(entry);
            return res;
        }

        public bool setRPAmount(int RPID, int level, int amount)
        {
            string entry = "global:resource:id" + RPID;
            string data = "level:" + level + "|amount:" + amount;
            return redis.Set(entry, data);
        }

        public string getRPAmount(int RPID)
        {
            string res = "";
            string entry = "global:resource:id" + RPID;
            if (redis.Exists(entry) == 0)
                redis.Set(entry, "level:1|amount:"+MinValueOfRP);
            res = redis.Get<string>(entry);
            return res;
        }

        public int getRPLevel(int RPID)
        {
            int res = 0;
            string entry = "global:resource:id" + RPID;
            if (redis.Exists(entry) != 0)
            {
                string strd = redis.Get<string>(entry);
                string[] stra = strd.Split('|');
                foreach (var strs in stra)
                {
                    string[] spl = strs.Split(':');
                    if (spl[0] == "level")
                    {
                        res = Int32.Parse(spl[1]);
                        break;
                    }
                }
            }
            return res;
        }
 
        public void setUserTimedData(string user, int RPID, string data, int secs)
        {
            string entry = "user:" + user + ":res" + RPID + ":timer";
            redis.Set(entry, Encoding.UTF8.GetBytes(data), secs);
        }

        public string getUserTimedData(string user, int RPID)
        {
            string res = "";
            string entry = "user:" + user + ":res" + RPID + ":timer";
            if (redis.Exists(entry) != 0)
            {
                byte[] bajtovi = redis.Get(entry);
                res = Encoding.UTF8.GetString(bajtovi);
            }
            return res;
        }

        public bool setRPContribution(int RPID, int amount)
        {
            string entry = "global:resource:id" + RPID + ":contribution";
            return redis.Set(entry, amount);
        }

        public int getRPContribution(int RPID)
        {
            int res = 0;
            string entry = "global:resource:id"+RPID+":contribution";
            if (redis.Exists(entry) == 0)
                redis.Set(entry, "0");
            string str = redis.Get<string>(entry);
            res = Int32.Parse(str);
            return res;
        }

        public bool setWorkerUpgradeData(string user, string data)
        {
            string entry = "user:"+user+":data:workerUpgrades";
            return redis.Set(entry, data);
        }

        public string getWorkerUpgradeData(string user)
        {
            string res = "";
            string entry = "user:" + user + ":data:workerUpgrades";
            if (redis.Exists(entry) == 0)
                redis.Set(entry, "");
            res = redis.Get<string>(entry);
            return res;
        }

        #endregion

    }
}
