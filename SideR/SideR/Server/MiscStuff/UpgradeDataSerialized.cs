﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Server.Entities;

namespace Server.MiscStuff
{
    // Same as UpgradeData, but it's used for data serialization. Unlike UpgradeData, it contains a direct list of Prices
    [Serializable]
    public class UpgradeDataSerialized
    {
        [XmlAttribute("upgrade_name")]
        public string name;

        [XmlAttribute("max_level")]
        public int maxLevel;
        
        [XmlArray("Upgrade_levels")]
        public List<LevelPrice> pricePerLevel;

        public UpgradeDataSerialized()
        {
            name = "";
            maxLevel = 0;
            pricePerLevel = new List<LevelPrice>();
        }

        public void Add(LevelPrice lp)
        {
            pricePerLevel.Add(lp);
        }

        public ItemPrice getPriceOfLevel(int id)
        {
            for(int i=0; i < pricePerLevel.Count(); i++)
            {
                if (pricePerLevel[i].Level==id)
                {
                    return new ItemPrice(pricePerLevel[i].prices);
                }
            }
            return null;
        }
    }
}
