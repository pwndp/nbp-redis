﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Server.MiscStuff
{
    // Contains the prices and level of an upgrade. Used for data serialization.
    [Serializable]
    public class LevelPrice
    {
        [XmlAttribute("upgrade_level")]
        public int Level { get; set; }

        [XmlArray("Prices")]
        [XmlArrayItem("Price")]
        public List<Price> prices;

        public LevelPrice()
        {
            prices = new List<Price>();
            Level = 0;
        }

        public void Add(Price p)
        {
            prices.Add(p);
        }
    }
}
