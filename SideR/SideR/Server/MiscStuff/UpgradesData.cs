﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Server.Entities;

namespace Server.MiscStuff
{
    // Used for saving and loading data about upgrades  
    [Serializable]
    [XmlRoot("PlayerUpgrades")]
    public class UpgradesData: XMLDataWorker
    {
        #region Singleton

        private static UpgradesData _inst = null;
        public static UpgradesData Instance
        {
            get
            {
                if (_inst == null)
                    _inst = new UpgradesData();
                return _inst;
            }
        }

        private UpgradesData()
        {
            SetFileName(@"Requirements\upgradesData.xml");
        }

        #endregion

        #region Attributes
        
        [XmlArray("UpgradesData")]
        [XmlArrayItem("Upgrade")]
        public List<UpgradeDataSerialized> data { get; set; }

        #endregion

        #region Functions

        public override void OnLoad(object xmlSerializer)
        {
            this.data = ((UpgradesData)xmlSerializer).data;
        }

        public Dictionary<string, UpgradeData> getData()
        {
            Load();
            return ConvertToDictionary();
        }

        // Converts the list of serialized upgrade data (different format than upgrade data) to a dictionary for later use
        private Dictionary<string,UpgradeData> ConvertToDictionary()
        {
            Dictionary<string, UpgradeData> dic = new Dictionary<string, UpgradeData>();
            foreach (UpgradeDataSerialized uds in this.data)
            {
                if (!dic.ContainsKey(uds.name))
                {
                    UpgradeData udata = new UpgradeData();
                    udata.maxLevel = uds.maxLevel;
                    foreach (LevelPrice lp in uds.pricePerLevel)
                        udata.prices.Add(lp.Level, new Entities.ItemPrice(lp.prices));
                    dic.Add(uds.name, udata);
                }
            }
            return dic;
        }

        #endregion

    }
}
