﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Server.MiscStuff
{
    [Serializable]
    public class ResourcePoolsData:XMLDataWorker
    {
        [XmlArray("ResourcePools")]
        [XmlArrayItem("ResourcePool")]
        public List<ResourcePool> resourcePools { get; set; }
        
        public ResourcePoolsData()
        {
            SetFileName(@"Requirements\resourcePoolsData.xml");
            resourcePools = new List<ResourcePool>();
        }

        public override void OnLoad(object xmlSerializer)
        {
            resourcePools = ((ResourcePoolsData)xmlSerializer).resourcePools;
        }

        public void Add(ResourcePool rp)
        {
            resourcePools.Add(rp);
        }

        public List<ResourcePool> GetData()
        {
            Load();
            return this.resourcePools;
        }
    }
}
