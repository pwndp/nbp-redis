﻿using Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.MiscStuff
{
    // Used to pass and handle data (for easier use by user) of an Upgrade
    public class UpgradeData
    {
        public int maxLevel = 0;
        public Dictionary<int, ItemPrice> prices = new Dictionary<int, ItemPrice>();
    }
}
