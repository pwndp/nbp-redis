﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Server.Entities;

namespace Server
{
    // IMC - Main class interface
    [ServiceContract]
    public interface IMC
    {
        [OperationContract]
        void TestFunc();


        #region Getter functions

        [OperationContract]
        ItemPrice getItemPrice(string upgradeName, int upgradeLevel);

        [OperationContract]
        List<ResourcePool> GetResourcePools();

        [OperationContract]
        ResourcePool MiningDonating(int idRP, int amount, bool mode);

        [OperationContract]
        bool SetUserTimedData(string username, int poolID, int workers, DateTime dt);

        [OperationContract]
        DateTime GetUserTimedData(string username, int poolID);

        [OperationContract]
        string GetAllResources();

        [OperationContract]
        string GetResources(int idRP);

        [OperationContract]
        int GetResourcePoolLevel(int poolID);

        [OperationContract]
        int GetRemainingRPContributions(int poolID);

        [OperationContract]
        bool SetRPContribution(int poolID, int amnt);

        [OperationContract]
        bool SetWorkerUpgradeData(string username, string data);

        [OperationContract]
        string GetWorkerUpgradeData(string username);

        [OperationContract]
        int getWorkerUpgradeDataValue(string username, string upgradeName);

        [OperationContract]
        int getRPPerWorkerUpgrade(string username, string name);

        [OperationContract]
        int GetUserBussyWorkers(string username, int poolID);

        [OperationContract]
        int GetUserBussyWorkersAll(string username);

        #endregion

        #region User functions

        [OperationContract]
        bool registerUser(string username, string password);

        [OperationContract]
        bool checkUser(string username, string password);

        [OperationContract]
        bool setUserData(string username, string data);

        [OperationContract]
        string getUserData(string username);

        [OperationContract]
        bool setUserUpgrades(string username, string data);

        [OperationContract]
        List<Upgrade> getUserUpgrades(string username);

        [OperationContract]
        int getUserUpgradeLevel(string username, string upgradeName);

        #endregion

    }
}
